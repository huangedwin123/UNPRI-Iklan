package unpri.skripsi.iklan.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import unpri.skripsi.iklan.Constants;
import unpri.skripsi.iklan.R;
import unpri.skripsi.iklan.activities.Account.Register;
import unpri.skripsi.iklan.activities.DoorsmeerDetail;
import unpri.skripsi.iklan.adapters.SearchDoorsmeerAdapter;
import unpri.skripsi.iklan.callbacks.DoorsmeerCallback;
import unpri.skripsi.iklan.callbacks.SearchDoorsmeerCallback;
import unpri.skripsi.iklan.controllers.DoorsmeerController;
import unpri.skripsi.iklan.controllers.UserController;
import unpri.skripsi.iklan.dao.DBController;
import unpri.skripsi.iklan.interfaceclass.DoorsmeerMapInterface;
import unpri.skripsi.iklan.interfaceclass.SearchDoorsmeerInterface;
import unpri.skripsi.iklan.io.APIErrorCallback;
import unpri.skripsi.iklan.models.IklanData;

/**
 * Created by Huang on 4/23/2017.
 */

public class DoorsmeerSearchFragment extends Fragment implements SearchDoorsmeerInterface, DoorsmeerMapInterface {
    private EditText atv_places;
    private ImageView ic_clear;
    private ListView doorsmeer_list;
    private LinearLayout loadinglayout;
    private SearchDoorsmeerAdapter searchDoorsmeerAdapter;
    private DBController dbController;
    private DoorsmeerController doorsmeerController;
    private final long DELAY = 1000; // milliseconds
    String keyword="";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.doorsmeer_search_fragment, container, false);
        atv_places = (EditText) rootView.findViewById(R.id.atv_places);
        atv_places.addTextChangedListener(atvPlacesTextWatcher);
        ic_clear = (ImageView) rootView.findViewById(R.id.ic_clear);
        doorsmeer_list = (ListView) rootView.findViewById(R.id.doorsmeer_list);
        doorsmeer_list.setOnItemClickListener(listItemClickListener);
        loadinglayout = (LinearLayout) rootView.findViewById(R.id.loadinglayout);
        dbController = new DBController(getActivity());
        ic_clear.setOnClickListener(clear);
        doorsmeerController = DoorsmeerController.getInstance(getActivity());
        doorsmeerController.setSearchDoorsmeerInterface(this);
        if(searchDoorsmeerAdapter==null) {
            searchDoorsmeerAdapter = new SearchDoorsmeerAdapter(getActivity());
        }
        loadinglayout.setVisibility(View.VISIBLE);
        keyword="";
        searchDoorsmeerAdapter.clearItem();
        searchDoorsmeer(keyword.toString());
        return rootView;
    }
    public View.OnClickListener clear = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            atv_places.setText("");
            ic_clear.setVisibility(View.GONE);
        }
    };
    private Timer timer=new Timer();
    public TextWatcher atvPlacesTextWatcher = new TextWatcher(){

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(count>0){
                ic_clear.setVisibility(View.VISIBLE);
            }else {
                ic_clear.setVisibility(View.GONE);
            }
        }
        @Override
        public void afterTextChanged(final Editable input) {
            timer.cancel();
            timer = new Timer();
            timer.schedule(new TimerTask() {

                public void run() {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(input.length()>1){
                                loadinglayout.setVisibility(View.VISIBLE);
                                String lastCharacterLocation =input.toString().substring(input.length()-1,input.length());
                                if(lastCharacterLocation.equals(" ")){
                                    keyword = input.toString().substring(0, input.length()-1);
                                }
                                else {
                                    keyword = input.toString();
                                }
                                searchDoorsmeerAdapter.clearItem();
                                searchDoorsmeer(keyword.toString());
                            }
                            else if(input.length()==0){
                                loadinglayout.setVisibility(View.VISIBLE);
                                keyword="";
                                searchDoorsmeerAdapter.clearItem();
                                searchDoorsmeer(keyword.toString());
//                                loadinglayout.setVisibility(View.GONE);
//                                searchDoorsmeerAdapter.removeNoResultView();

                            }
                        }
                    });
                }
            }, DELAY);

        }
    };
    private AdapterView.OnItemClickListener listItemClickListener = new AdapterView.OnItemClickListener(){

        @Override
        public void onItemClick(AdapterView<?> parent, View view,
                                int position, long id) {
            IklanData IklanData = searchDoorsmeerAdapter.getItemdata().get(position);
            Intent detail = new Intent(getActivity(), DoorsmeerDetail.class);
            detail.putExtra("id", IklanData.id);
            getActivity().startActivity(detail);
        }
    };
    public void SetListViewAdapter(ArrayList<IklanData> IklanDatas) {
        loadinglayout.setVisibility(View.GONE);
        if(searchDoorsmeerAdapter==null) {
            searchDoorsmeerAdapter = new SearchDoorsmeerAdapter(getActivity());
        }
        searchDoorsmeerAdapter.removeNoResultView();
        searchDoorsmeerAdapter.clearItem();
        searchDoorsmeerAdapter.addItem(IklanDatas);
        doorsmeer_list.setAdapter(searchDoorsmeerAdapter);
    }


    private void searchDoorsmeer(String keyword){

        DoorsmeerController.getInstance(getActivity()).searchDoorsmeer(searchDoorsmeerParams(keyword));
        return;
    }
    public Map<String, String> searchDoorsmeerParams(String keyword) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("keyword", keyword);
        return  params;
    }

    @Override
    public void OnSuccessSearchDoorsmeer(SearchDoorsmeerCallback searchDoorsmeerCallback) {
        loadinglayout.setVisibility(View.GONE);
        int sukses = searchDoorsmeerCallback.getSukses();
        if(sukses==1){
            ArrayList<IklanData> IklanDatas = searchDoorsmeerCallback.getDoorsmeer_data();
            if(IklanDatas!=null&&IklanDatas.size()!=0){
                SetListViewAdapter(IklanDatas);
            }else{
                searchDoorsmeerAdapter.removeNoResultView();
                searchDoorsmeerAdapter.setNoResultView();
                searchDoorsmeerAdapter.clearItem();
            }
        }else{

        }
    }

    @Override
    public void OnErrorSearchDoorsmeer(APIErrorCallback apiErrorCallback) {

    }

    @Override
    public void onSuccessGetDoorsmeerMap(DoorsmeerCallback doorsmeerCallback) {
        int sukses = doorsmeerCallback.getSukses();
        if(sukses==1){
            ArrayList<IklanData> IklanDatas = doorsmeerCallback.getData();
            if(IklanDatas.size()!=0){
                SetListViewAdapter(IklanDatas);
            }else{
                searchDoorsmeerAdapter.removeNoResultView();
                searchDoorsmeerAdapter.setNoResultView();
                searchDoorsmeerAdapter.clearItem();
            }
        }
    }

    @Override
    public void onErrorGetDoorsmeerMap(APIErrorCallback apiErrorCallback) {

    }
}
