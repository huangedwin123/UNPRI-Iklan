package unpri.skripsi.iklan.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import unpri.skripsi.iklan.R;
import unpri.skripsi.iklan.adapters.ProfileMenuAdapter;
import unpri.skripsi.iklan.models.ProfileMenuData;
import unpri.skripsi.iklan.utility.SessionManager;

/**
 * Created by Huang on 4/24/2017.
 */

public class ProfileFragment extends Fragment{
    private RecyclerView menuList;
    private ProfileMenuAdapter profileMenuAdapter;
    private SessionManager sessionManager;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.profile_fragment, container, false);
        initSession();
        renderViews(rootView);
        SetListViewAdapter(initProfileMenu());
        return rootView;
    }

    private void initSession(){
        sessionManager = new SessionManager(getActivity());
    }
    private void renderViews(View rootView){
        menuList = (RecyclerView) rootView.findViewById(R.id.menuList);
        menuList.setHasFixedSize(true);
        menuList.setNestedScrollingEnabled(false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        menuList.setLayoutManager(linearLayoutManager);
    }
    public void SetListViewAdapter(List<ProfileMenuData> profileMenuDatas) {
        menuList.setVisibility(View.VISIBLE);
        profileMenuAdapter = new ProfileMenuAdapter(getActivity(),profileMenuDatas);
        menuList.setAdapter(profileMenuAdapter);
    }

    private ArrayList<ProfileMenuData> initProfileMenu(){
        ArrayList<ProfileMenuData> profileMenuDatas = new ArrayList<>();
            ProfileMenuData firstMenu = new ProfileMenuData();
            firstMenu.setMenuIcon(R.mipmap.ic_launcher);
            firstMenu.setMenuName("Billboard");
            ProfileMenuData secondMenu = new ProfileMenuData();
            secondMenu.setMenuIcon(R.mipmap.ic_launcher);
            secondMenu.setMenuName("Baliho");
            ProfileMenuData thirdMenu = new ProfileMenuData();
            thirdMenu.setMenuIcon(R.mipmap.ic_launcher);
            thirdMenu.setMenuName("TV");

            profileMenuDatas.add(firstMenu);
            profileMenuDatas.add(secondMenu);
            profileMenuDatas.add(thirdMenu);

        return profileMenuDatas;
    }
}
