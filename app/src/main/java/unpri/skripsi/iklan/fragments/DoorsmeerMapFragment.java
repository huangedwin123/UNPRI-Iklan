package unpri.skripsi.iklan.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import unpri.skripsi.iklan.Config;
import unpri.skripsi.iklan.Constants;
import unpri.skripsi.iklan.R;
import unpri.skripsi.iklan.activities.DoorsmeerDetail;
import unpri.skripsi.iklan.adapters.SearchDoorsmeerAdapter;
import unpri.skripsi.iklan.callbacks.DirectionRouteGMapsCallback;
import unpri.skripsi.iklan.callbacks.DoorsmeerCallback;
import unpri.skripsi.iklan.controllers.DoorsmeerController;
import unpri.skripsi.iklan.controllers.UtilityController;
import unpri.skripsi.iklan.interfaceclass.DirectionRouteGmapsInterface;
import unpri.skripsi.iklan.interfaceclass.DoorsmeerMapInterface;
import unpri.skripsi.iklan.io.APIErrorCallback;
import unpri.skripsi.iklan.models.DirectionRouteGmapsData.Legs;
import unpri.skripsi.iklan.models.DirectionRouteGmapsData.PolylineData;
import unpri.skripsi.iklan.models.DirectionRouteGmapsData.Routes;
import unpri.skripsi.iklan.models.DirectionRouteGmapsData.Steps;
import unpri.skripsi.iklan.models.IklanData;
import unpri.skripsi.iklan.utility.PiccassoUtility;
import unpri.skripsi.iklan.utility.Utility;
import unpri.skripsi.iklan.views.CustomViewPager;

/**
 * Created by Huang on 4/23/2017.
 */

public class DoorsmeerMapFragment extends Fragment implements OnMapReadyCallback, DoorsmeerMapInterface {
    private View view;
    private GoogleMap googleMap;
    private MapFragment fm;
    private double lat_constant;
    private double lng_constant;
    private String markerId;
    private String markerMyselfId;
    public Marker doorsmeerMarker;
    private DoorsmeerController doorsmeerController;
    private ArrayList<IklanData> IklanDatas;
    private ImageButton bt_route;
    private RelativeLayout bt_route_layout;
    private ImageButton bt_distance;
    private RelativeLayout bt_distance_layout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.doorsmeer_map_fragment, container, false);
            Log.d("Log", "Tryed");
        } catch (InflateException e) {
            Log.d("Log", "crashed "+e);
        }
        colorNames.add("Merah");
        colorNames.add("Biru");
        colorNames.add("Hijau");
        colorNames.add("Hitam");
        colorArray.add(Color.RED);
        colorArray.add(Color.BLUE);
        colorArray.add(Color.GREEN);
        colorArray.add(Color.BLACK);
        doorsmeerController = DoorsmeerController.getInstance(getActivity());
        doorsmeerController.setDoorsmeerMapInterface(this);
        renderViews(view);
        getIntentExtras();
        initializeMap();
        return view;
    }

    private void initializeMap() {
        fm = (MapFragment) getActivity().getFragmentManager().findFragmentById(R.id.map);
        fm.getMapAsync(this);
    }

    private void renderViews(View view){
        bt_route = (ImageButton) view.findViewById(R.id.bt_route);
        bt_route_layout = (RelativeLayout) view.findViewById(R.id.bt_route_layout);
        bt_distance = (ImageButton) view.findViewById(R.id.bt_distance);
        bt_distance_layout = (RelativeLayout) view.findViewById(R.id.bt_distance_layout);
        bt_route.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("CLICKEd","asdfas");
                getDirectionRoute(String.valueOf(lat_constant), String.valueOf(lng_constant),
                        latMarkerClicked,lngMarkerClicked);
            }
        });
    }
    String latMarkerClicked ="";
    String lngMarkerClicked ="";
    private void setDoorsmeerMarker(List<IklanData> IklanDatas) {
        if(IklanDatas!=null&&IklanDatas.size()!=0) {
            for (int i = 0; i < IklanDatas.size(); i++) {
                MarkerOptions markerRestaurantOption = new MarkerOptions().position(
                        new LatLng(IklanDatas.get(i).latitude, IklanDatas.get(i).longitude))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_from_place))
                        .title(String.valueOf(i));
                doorsmeerMarker = googleMap.addMarker(markerRestaurantOption);
            }
        }else{
        }
    }

    private void getDoorsmeerMapData(){
        DoorsmeerController.getInstance(getActivity()).getIklanData();
        return;
    }


    
    Marker lastOpenned = null;
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        getDoorsmeerMapData();
//        final List<IklanData> IklanDatas = setOrphanageList();
//        setDoorsmeerMarker(IklanDatas);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat_constant, lng_constant), 13.0f));
        MarkerOptions markerMyselfOption = new MarkerOptions().position(
                new LatLng(lat_constant, lng_constant)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_from));
        Marker markerMyself = googleMap.addMarker(markerMyselfOption);
        markerMyselfId = markerMyself.getId();

        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            // Use default InfoWindow frame
            @Override
            public View getInfoWindow(final Marker marker) {
                String markerId = marker.getId();
                Log.d("markerId",""+markerId);
                Log.d("markerDriverId",""+markerMyselfId);
//                if(markerMyselfId.equals(markerId)) {
//                    return null;
//                }
//                else{
                doorsmeerMarker = marker;
                // Getting view from the layout file info_window_layout;
                View v = getActivity().getLayoutInflater().inflate(R.layout.custom_marker, null);
                TextView alamat_ads= (TextView)v.findViewById(R.id.alamat_ads);
                ImageView foto_ads= (ImageView)v.findViewById(R.id.foto_ads);
                
                TextView jenis_ads= (TextView)v.findViewById(R.id.jenis_ads);
                String title = doorsmeerMarker.getTitle();
                IklanData data = IklanDatas.get((Integer.parseInt(title)));
                String urlImage = Config.APP_IMAGE_URL+data.cover_image;
                PiccassoUtility.loadImage(getActivity(),urlImage , foto_ads);

                alamat_ads.setText(data.alamat);
                jenis_ads.setText(data.nama_jenis);
                return v;
//                }
            }
            // Defines the contents of the InfoWindow
            @Override
            public View getInfoContents(Marker marker) {
                if (doorsmeerMarker != null
                        && doorsmeerMarker.isInfoWindowShown()) {
                    doorsmeerMarker.hideInfoWindow();
                    doorsmeerMarker.showInfoWindow();
                }
                return null;
            }
        });
        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            public boolean onMarkerClick(Marker marker) {
                // Check if there is an open info window
                if (lastOpenned != null) {
                    // Close the info window
                    lastOpenned.hideInfoWindow();

                    // Is the marker the same marker that was already open
                    if (lastOpenned.equals(marker)) {
                        // Nullify the lastOpenned object
                        lastOpenned = null;
                        // Return so that the info window isn't openned again
                        return true;
                    }
                }

                if(!marker.getId().equals(markerMyselfId)) {
                    // Open the info window for the marker
                    marker.showInfoWindow();
                    lastOpenned = marker;
                    bt_route_layout.setVisibility(View.VISIBLE);
                    latMarkerClicked = String.valueOf(marker.getPosition().latitude);
                    lngMarkerClicked = String.valueOf(marker.getPosition().longitude);
                }
                // Re-assign the last openned such that we can close it later


                // Event  was handled by our code do not launch default behaviour.
                return true;
            }
        });
        googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener()
        {
            @Override
            public void onInfoWindowClick(Marker marker) {
                IklanData intentIklanData = IklanDatas.get(Utility.getInstance().parseInteger(marker.getTitle()));
                Intent detail = new Intent(getActivity(), DoorsmeerDetail.class);
                detail.putExtra("id", intentIklanData.id);
                getActivity().startActivity(detail);
            }

        });

    }

    public void getIntentExtras() {
        Bundle bundle = this.getArguments();
        lat_constant =bundle.getDouble("latitude");
        lng_constant=bundle.getDouble("longitude");
    }
    public void getDirectionRoute(String latitude_from, String longitude_from, String latitude_to, String longitude_to) {
        //String api = Utility.getInstance().getTokenApi(BookingInProgressDetailMember.this);
        //String output = "json";
        String origin = latitude_from + "," + longitude_from;
        String destinations = latitude_to + "," + longitude_to;
        UtilityController.getInstance(getActivity()).getDirectionRoute(directionParameters(origin, destinations),directionRouteGmapsInterface);
        return;
    }
    ArrayList<Integer> colorArray = new ArrayList<>();
    ArrayList<String> colorNames= new ArrayList<>();
    ArrayList<Polyline> polylineArray = new ArrayList<>();
    List<Routes> routesArray;
    private Polyline polyline;
    List<Legs> legsArray;
    DirectionRouteGMapsCallback directionRouteGMaps;
    private DirectionRouteGmapsInterface directionRouteGmapsInterface = new DirectionRouteGmapsInterface() {
        @Override
        public void onSuccessGetRoute(DirectionRouteGMapsCallback directionRouteGMapsCallback) {
            String status = directionRouteGMapsCallback.getStatus();
            if (status.equals("OK")) {
                directionRouteGMaps = directionRouteGMapsCallback;
                if(polylineArray!=null&&polylineArray.size()!=0){
                    for(int i = 0; i<polylineArray.size(); i++){
                        polylineArray.get(i).remove();
                    }
                }
                List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String, String>>>();
                routesArray = directionRouteGMaps.getRoutes();

                directionRouteGMaps.setColorsArray(colorArray);
                directionRouteGMaps.setColorsName(colorNames);
                PolylineOptions lineOptions = null;
                for (int i = 0; i < routesArray.size(); i++) {
                    ArrayList<LatLng> list = new ArrayList<LatLng>();
                    legsArray = routesArray.get(i).getLegs();
                    List path = new ArrayList<HashMap<String, String>>();

                    /** Traversing all legs */
                    for (int j = 0; j < legsArray.size(); j++) {
                        List<Steps> stepsArray = legsArray.get(j).getSteps();
                        /** Traversing all steps */
                        for (int k = 0; k < stepsArray.size(); k++) {
                            lineOptions = new PolylineOptions();
                            PolylineData polyline = stepsArray.get(k).getPolyline();
                            String points = polyline.getPoints();
                            list.addAll(decodePoly(points));
                            /** Traversing all points */
                        }
                    }
                    lineOptions.addAll(list);
                    lineOptions.width(5);
                    lineOptions.color(colorArray.get(i));
                    polylineArray.add(googleMap.addPolyline(lineOptions));
                }

            } else {
                Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
            }
            bt_distance_layout.setVisibility(View.VISIBLE);
        }

        @Override
        public void onErrorGetRoute(APIErrorCallback apiErrorCallback) {

        }
    };
    public Map<String, String> directionParameters(String origins, String destinations) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("origin", origins);
        params.put("destination", destinations);
        params.put("sensor", "false");
        params.put("language", "id");
        return params;
    }

    private ArrayList<LatLng> decodePoly(String encoded) {

        ArrayList<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }
    @Override
    public void onSuccessGetDoorsmeerMap(DoorsmeerCallback doorsmeerCallback) {
        int sukses = doorsmeerCallback.getSukses();
        if(sukses==1){
            IklanDatas = doorsmeerCallback.getData();
            setDoorsmeerMarker(IklanDatas);
        }
    }

    @Override
    public void onErrorGetDoorsmeerMap(APIErrorCallback apiErrorCallback) {

    }
}
