package unpri.skripsi.iklan.callbacks;

import java.util.ArrayList;

/**
 * Created by Huang on 5/25/2017.
 */

public class RegisterCallback extends BaseCallback{
    private ArrayList<String> error_form;
    String data;

    public ArrayList<String> getError() {
        return error_form;
    }

    public void setError(ArrayList<String> error_form) {
        this.error_form = error_form;
    }

//	public VerificationData getVerificationData() {
//		return data;
//	}
//
//	public void setVerificationData(VerificationData data) {
//		this.data = data;
//	}

    public String getToken() {
        return data;
    }

    public void setToken(String token) {
        this.data = data;
    }
}
