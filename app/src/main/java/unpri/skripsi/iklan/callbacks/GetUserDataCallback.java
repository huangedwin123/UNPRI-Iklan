package unpri.skripsi.iklan.callbacks;

import unpri.skripsi.iklan.models.GetUserData;

/**
 * Created by Huang on 5/28/2017.
 */

public class GetUserDataCallback {
    GetUserData data;

    public GetUserData getUserData(){
        return data;
    }
    public void setUserData(GetUserData data){
        this.data  = data;
    }
}
