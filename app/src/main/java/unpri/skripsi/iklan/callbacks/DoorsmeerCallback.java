package unpri.skripsi.iklan.callbacks;

import java.util.ArrayList;

import unpri.skripsi.iklan.models.IklanData;

/**
 * Created by Huang on 6/18/2017.
 */

public class DoorsmeerCallback extends BaseCallback{
    ArrayList<IklanData> data;

    public ArrayList<IklanData> getData() {
        return data;
    }

    public void setData(ArrayList<IklanData> data) {
        this.data = data;
    }
}
