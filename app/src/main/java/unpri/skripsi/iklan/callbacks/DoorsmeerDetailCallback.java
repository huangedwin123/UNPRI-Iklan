package unpri.skripsi.iklan.callbacks;

import unpri.skripsi.iklan.models.IklanDetailData;

/**
 * Created by Huang on 7/31/2017.
 */

public class DoorsmeerDetailCallback extends BaseCallback{
    IklanDetailData data;

    public IklanDetailData getData() {
        return data;
    }

    public void setData(IklanDetailData data) {
        this.data = data;
    }
}
