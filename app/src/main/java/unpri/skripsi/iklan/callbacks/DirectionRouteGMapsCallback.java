package unpri.skripsi.iklan.callbacks;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import unpri.skripsi.iklan.models.DirectionRouteGmapsData.Routes;

/**
 * Created by Huang on 8/6/2017.
 */

public class DirectionRouteGMapsCallback implements Parcelable{
    String callback;
    ArrayList<Integer> colorsArray;
    ArrayList<String> colorsName;

    protected DirectionRouteGMapsCallback(Parcel in) {
        callback = in.readString();
        routes = in.createTypedArrayList(Routes.CREATOR);
        status = in.readString();
        colorsArray = new ArrayList<>();
        in.readList(colorsArray, Integer.class.getClassLoader());
        colorsName = new ArrayList<String>();
        in.readStringList(colorsName);
    }

    public static final Creator<DirectionRouteGMapsCallback> CREATOR = new Creator<DirectionRouteGMapsCallback>() {
        @Override
        public DirectionRouteGMapsCallback createFromParcel(Parcel in) {
            return new DirectionRouteGMapsCallback(in);
        }

        @Override
        public DirectionRouteGMapsCallback[] newArray(int size) {
            return new DirectionRouteGMapsCallback[size];
        }
    };

    public String getCallback() {
        return callback;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }

    @SerializedName("routes")
    @Expose
    private List<Routes> routes = new ArrayList<Routes>();
    @SerializedName("status")
    @Expose
    private String status;

    /**
     *
     * @return
     * The rows
     */
    public List<Routes> getRoutes() {
        return routes;
    }

    /**
     *
     * @param routes
     * The rows
     */
    public void setRoute(List<Routes> routes) {
        this.routes = routes;
    }

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(callback);
        dest.writeTypedList(routes);
        dest.writeString(status);
        dest.writeList(colorsArray);
        dest.writeStringList(colorsName);
    }

    public ArrayList<Integer> getColorsArray() {
        return colorsArray;
    }

    public void setColorsArray(ArrayList<Integer> colorsArray) {
        this.colorsArray = colorsArray;
    }

    public ArrayList<String> getColorsName() {
        return colorsName;
    }

    public void setColorsName(ArrayList<String> colorsName) {
        this.colorsName = colorsName;
    }

    public void setRoutes(List<Routes> routes) {
        this.routes = routes;
    }
}