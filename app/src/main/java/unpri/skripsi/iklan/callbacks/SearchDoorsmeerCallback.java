package unpri.skripsi.iklan.callbacks;

import java.util.ArrayList;

import unpri.skripsi.iklan.models.IklanData;

/**
 * Created by edwin on 20/07/2017.
 */

public class SearchDoorsmeerCallback extends BaseCallback{
    ArrayList<IklanData> data;

    public ArrayList<IklanData> getDoorsmeer_data() {
        return data;
    }

    public void setDoorsmeer_data(ArrayList<IklanData> data) {
        this.data = data;
    }
}
