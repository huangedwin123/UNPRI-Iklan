package unpri.skripsi.iklan.callbacks;

import java.util.ArrayList;

import unpri.skripsi.iklan.models.IklanData;

/**
 * Created by Huang on 8/1/2017.
 */

public class AdsTipeListCallback extends BaseCallback{
    ArrayList<IklanData> data;

    public ArrayList<IklanData> getAdsTipeList() {
        return data;
    }

}
