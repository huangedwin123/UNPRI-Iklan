package unpri.skripsi.iklan.callbacks;

import java.util.ArrayList;

import unpri.skripsi.iklan.models.GetUserData;


public class LoginCallback extends BaseCallback{
    GetUserData userData;

    public GetUserData getUserData() {
        return userData;
    }

    public void setUserData(GetUserData userData) {
        this.userData = userData;
    }
}
