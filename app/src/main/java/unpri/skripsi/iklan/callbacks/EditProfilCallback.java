package unpri.skripsi.iklan.callbacks;

import java.util.ArrayList;

/**
 * Created by Huang on 5/25/2017.
 */

public class EditProfilCallback extends BaseCallback{
    ArrayList<String> error;
    String data;

    public ArrayList<String>getError() {
        return error;
    }

    public void setError(ArrayList<String> error) {
        this.error = error;
    }

    public String getToken() {
        return data;
    }

    public void setToken(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

}
