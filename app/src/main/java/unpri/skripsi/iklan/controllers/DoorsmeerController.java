package unpri.skripsi.iklan.controllers;

import android.content.Context;
import android.util.Log;

import com.squareup.otto.Produce;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import unpri.skripsi.iklan.R;
import unpri.skripsi.iklan.callbacks.AdsTipeListCallback;
import unpri.skripsi.iklan.callbacks.DoorsmeerCallback;
import unpri.skripsi.iklan.callbacks.DoorsmeerDetailCallback;
import unpri.skripsi.iklan.callbacks.RegisterCallback;
import unpri.skripsi.iklan.callbacks.SearchDoorsmeerCallback;
import unpri.skripsi.iklan.interfaceclass.AdsTipeInterface;
import unpri.skripsi.iklan.interfaceclass.DoorsmeerDetailInterface;
import unpri.skripsi.iklan.interfaceclass.DoorsmeerMapInterface;
import unpri.skripsi.iklan.interfaceclass.OnLoginListener;
import unpri.skripsi.iklan.interfaceclass.SearchDoorsmeerInterface;
import unpri.skripsi.iklan.io.APIErrorCallback;
import unpri.skripsi.iklan.io.ApiInterface;
import unpri.skripsi.iklan.io.BusProvider;
import unpri.skripsi.iklan.io.ErrorUtils;
import unpri.skripsi.iklan.io.RestPhp;

/**
 * Created by Huang on 6/18/2017.
 */

public class DoorsmeerController extends BaseController {
    private static DoorsmeerController _instance;
    private Context context;
    private SearchDoorsmeerInterface searchDoorsmeerInterface;
    private DoorsmeerMapInterface doorsmeerMapInterface;
    private DoorsmeerDetailInterface doorsmeerDetailInterface;
    private AdsTipeInterface adsTipeInterface;

    protected DoorsmeerController(Context paramContext)
    {
        super(paramContext);
        this.context = paramContext;
    }

    public static DoorsmeerController getInstance(Context paramContext)
    {
        if(_instance==null) {
            _instance = new DoorsmeerController(paramContext);
        }
        return _instance;
    }


    public void setDoorsmeerMapInterface(DoorsmeerMapInterface doorsmeerMapInterface){
        this.doorsmeerMapInterface = doorsmeerMapInterface;

    }

    public void setSearchDoorsmeerInterface(SearchDoorsmeerInterface searchDoorsmeerInterface){
        this.searchDoorsmeerInterface = searchDoorsmeerInterface;

    }

    public void setDoorsmeerDetailInterface(DoorsmeerDetailInterface doorsmeerDetailInterface){
        this.doorsmeerDetailInterface = doorsmeerDetailInterface;
    }
    public void getAdsTipeInterface(AdsTipeInterface adsTipeInterface){
        this.adsTipeInterface = adsTipeInterface;
    }

    @Produce
    public void getIklanData(){
        ApiInterface service = RestPhp.getClient();
        Call<DoorsmeerCallback> call = service.getIklanData();
        call.enqueue(new Callback<DoorsmeerCallback>() {
            @Override
            public void onResponse(Call<DoorsmeerCallback> call, Response<DoorsmeerCallback> response) {
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    DoorsmeerCallback Data = response.body();
                    doorsmeerMapInterface.onSuccessGetDoorsmeerMap(Data);
                    Log.d("Success","success");
                } else {
                    APIErrorCallback error = ErrorUtils.parseError(response);
                    doorsmeerMapInterface.onErrorGetDoorsmeerMap(error);
                    Log.d("ERROR","error");
                }
            }

            @Override
            public void onFailure(Call<DoorsmeerCallback> call, Throwable t) {
                APIErrorCallback error =new APIErrorCallback();
                String error_msg = t.getMessage();
                Log.d("Failure","failure");
                if(error_msg!=null) {
                    error.setError(error_msg+"");
                }
                else{
                    error.setError(context.getResources().getString(R.string.error));
                }
                doorsmeerMapInterface.onErrorGetDoorsmeerMap(error);
            }
        });
    }

    @Produce
    public void searchDoorsmeer(final Map<String,String> params){
        ApiInterface service = RestPhp.getClient();
        Call<SearchDoorsmeerCallback> call = service.searchDoorsmeer(params);
        call.enqueue(new Callback<SearchDoorsmeerCallback>() {
            @Override
            public void onResponse(Call<SearchDoorsmeerCallback> call, Response<SearchDoorsmeerCallback> response) {
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    SearchDoorsmeerCallback Data = response.body();
                    searchDoorsmeerInterface.OnSuccessSearchDoorsmeer(Data);
                } else {
                    APIErrorCallback error = ErrorUtils.parseError(response);
                    searchDoorsmeerInterface.OnErrorSearchDoorsmeer(error);
                }
            }

            @Override
            public void onFailure(Call<SearchDoorsmeerCallback> call, Throwable t) {
                APIErrorCallback error =new APIErrorCallback();
                String error_msg = t.getMessage();
                if(error_msg!=null) {
                    error.setError(error_msg+"");
                }
                else{
                    error.setError(context.getResources().getString(R.string.error));
                }
                searchDoorsmeerInterface.OnErrorSearchDoorsmeer(error);
            }
        });
    }
    @Produce
    public void getAdsDetail(final Map<String,String> params){
        ApiInterface service = RestPhp.getClient();
        Call<DoorsmeerDetailCallback> call = service.getAdsDetail(params);
        call.enqueue(new Callback<DoorsmeerDetailCallback>() {
            @Override
            public void onResponse(Call<DoorsmeerDetailCallback> call, Response<DoorsmeerDetailCallback> response) {
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    DoorsmeerDetailCallback Data = response.body();
                    doorsmeerDetailInterface.onSuccessGetDoorsmeerDetail(Data);
                } else {
                    APIErrorCallback error = ErrorUtils.parseError(response);
                    doorsmeerDetailInterface.onErrorGetDoorsmeerDetail(error);
                }
            }

            @Override
            public void onFailure(Call<DoorsmeerDetailCallback> call, Throwable t) {
                APIErrorCallback error =new APIErrorCallback();
                String error_msg = t.getMessage();
                if(error_msg!=null) {
                    error.setError(error_msg+"");
                }
                else{
                    error.setError(context.getResources().getString(R.string.error));
                }
                doorsmeerDetailInterface.onErrorGetDoorsmeerDetail(error);
            }
        });
    }
    @Produce
    public void getAdsTipeList(final Map<String,String> params){
        ApiInterface service = RestPhp.getClient();
        Call<AdsTipeListCallback> call = service.getAdsTipeList(params);
        call.enqueue(new Callback<AdsTipeListCallback>() {
            @Override
            public void onResponse(Call<AdsTipeListCallback> call, Response<AdsTipeListCallback> response) {
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    AdsTipeListCallback Data = response.body();
                    adsTipeInterface.onSuccessAdsTipe(Data);
                } else {
                    APIErrorCallback error = ErrorUtils.parseError(response);
                    adsTipeInterface.onErrorAdsTipe(error);
                }
            }

            @Override
            public void onFailure(Call<AdsTipeListCallback> call, Throwable t) {
                APIErrorCallback error =new APIErrorCallback();
                String error_msg = t.getMessage();
                if(error_msg!=null) {
                    error.setError(error_msg+"");
                }
                else{
                    error.setError(context.getResources().getString(R.string.error));
                }
                adsTipeInterface.onErrorAdsTipe(error);
            }
        });
    }
}
