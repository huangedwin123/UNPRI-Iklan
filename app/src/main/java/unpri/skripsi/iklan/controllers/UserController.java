package unpri.skripsi.iklan.controllers;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import com.squareup.otto.Produce;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import unpri.skripsi.iklan.Config;
import unpri.skripsi.iklan.R;
import unpri.skripsi.iklan.callbacks.BaseCallback;
import unpri.skripsi.iklan.callbacks.EditProfilCallback;
import unpri.skripsi.iklan.callbacks.GetUserDataCallback;
import unpri.skripsi.iklan.callbacks.LoginCallback;
import unpri.skripsi.iklan.callbacks.RegisterCallback;
import unpri.skripsi.iklan.interfaceclass.OnLoginListener;
import unpri.skripsi.iklan.interfaceclass.OnRegisterListener;
import unpri.skripsi.iklan.io.APIErrorCallback;
import unpri.skripsi.iklan.io.ApiInterface;
import unpri.skripsi.iklan.io.BusProvider;
import unpri.skripsi.iklan.io.ErrorUtils;
import unpri.skripsi.iklan.io.RestClient;
import unpri.skripsi.iklan.io.RestPhp;
import unpri.skripsi.iklan.utility.SessionManager;


public class UserController extends BaseController {
    private static UserController _instance;
    private Context context;
    private RegisterCallback registerCallback;
    private OnRegisterListener onRegisterListener;
    private OnLoginListener onLoginListener;
    protected UserController(Context paramContext)
    {
        super(paramContext);
        this.context = paramContext;
    }

    public static UserController getInstance(Context paramContext)
    {
        if(_instance==null) {
            _instance = new UserController(paramContext);
        }
        return _instance;
    }
    public void setRegisterInterface(OnRegisterListener onRegisterListener){
        this.onRegisterListener = onRegisterListener;

    }
    public void setLoginInterface(OnLoginListener onLoginListener){
        this.onLoginListener = onLoginListener;

    }
    @Produce
    public void postLogin(final Map<String,String> params){
        ApiInterface service = RestPhp.getClient();
        Call<LoginCallback> call = service.postLogin(params);
        call.enqueue(new Callback<LoginCallback>() {
            @Override
            public void onResponse(Call<LoginCallback> call, Response<LoginCallback> response) {
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    LoginCallback Data = response.body();
                    onLoginListener.onLoginSuccess(Data);
                } else {
                    APIErrorCallback error = ErrorUtils.parseError(response);
                    onLoginListener.onLoginError(error);
                }
            }

            @Override
            public void onFailure(Call<LoginCallback> call, Throwable t) {

                APIErrorCallback error =new APIErrorCallback();
                String error_msg = t.getMessage();
                if(error_msg!=null) {
                    error.setError(error_msg+"");
                }
                else{
                    error.setError(context.getResources().getString(R.string.error));
                }
                onLoginListener.onLoginError(error);
            }
        });
    }

    @Produce
    public void postRegister(Map<String,String> params){
        ApiInterface service = RestPhp.getClient();
        Call<RegisterCallback> call = service.postRegister(params);
        call.enqueue(new Callback<RegisterCallback>() {
            @Override
            public void onResponse(Call<RegisterCallback> call, Response<RegisterCallback> response) {

                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    registerCallback = response.body();
                    onRegisterListener.onRegisterSuccess(registerCallback);

                } else {
                    APIErrorCallback error = ErrorUtils.parseError(response);
                    onRegisterListener.onRegisterError(error);
                }
            }

            @Override
            public void onFailure(Call<RegisterCallback> call, Throwable t) {
                APIErrorCallback error =new APIErrorCallback();
                String error_msg = t.getMessage();
                if(error_msg!=null) {
                    error.setError(error_msg+"");
                }
                else{
                    error.setError(context.getResources().getString(R.string.error));
                }
                onRegisterListener.onRegisterError(error);
            }
        });
    }
    @Produce
    public void postEditProfil(Map<String,String> params){
        ApiInterface service = RestPhp.getClient();
        Call<EditProfilCallback> call = service.postEditProfil(params);

        call.enqueue(new Callback<EditProfilCallback>() {
            @Override
            public void onResponse(Call<EditProfilCallback> call, Response<EditProfilCallback> response) {
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    EditProfilCallback baseCallback = response.body();
                    baseCallback.setCallback("postUserData");
                    BusProvider.getInstance().post(baseCallback);
                } else {
                    APIErrorCallback error = ErrorUtils.parseError(response);
                    error.setCallback("postUserData");
                    BusProvider.getInstance().post(error);
                }
            }
            @Override
            public void onFailure(Call<EditProfilCallback> call, Throwable t) {
                APIErrorCallback error =new APIErrorCallback();
                error.setCallback("postUserData");
                String error_msg = t.getMessage();
                if(error_msg!=null) {
                    error.setError(error_msg+"");
                }
                else{
                    error.setError(context.getResources().getString(R.string.error));
                }
                BusProvider.getInstance().post(error);
            }
        });
    }
    @Produce
    public void getUserData(String user_id){
        ApiInterface service = RestPhp.getClient();
        Call<GetUserDataCallback> call = service.getUserData(user_id);
        call.enqueue(new Callback<GetUserDataCallback>() {
            @Override
            public void onResponse(Call<GetUserDataCallback> call, Response<GetUserDataCallback> response) {
//				Log.d("booking", "raww = " + response.raw());
//				Log.d("booking", "getBookingTransportListDetail = " + new Gson().toJson(response.body()));
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    GetUserDataCallback getUserDataCallback = response.body();
//                    getUserDataCallback.setCallback("getUserData");
                    BusProvider.getInstance().post(getUserDataCallback);
                } else {
                    APIErrorCallback error = ErrorUtils.parseError(response);
                    error.setCallback("getUserData");
                    BusProvider.getInstance().post(error);
                }
            }
            @Override
            public void onFailure(Call<GetUserDataCallback> call, Throwable t) {
                APIErrorCallback error =new APIErrorCallback();
                error.setCallback("getUserData");
                String error_msg = t.getMessage();
                if(error_msg!=null) {
                    error.setError(error_msg+"");
                }
                else{
                    error.setError(context.getResources().getString(R.string.error));
                }
                BusProvider.getInstance().post(error);
            }
        });
    }
}
