package unpri.skripsi.iklan.controllers;

import android.app.ProgressDialog;
import android.content.Context;


import com.squareup.otto.Produce;

import java.util.HashMap;
import java.util.Map;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import unpri.skripsi.iklan.R;
import unpri.skripsi.iklan.callbacks.DirectionRouteGMapsCallback;
import unpri.skripsi.iklan.interfaceclass.DirectionRouteGmapsInterface;
import unpri.skripsi.iklan.io.APIErrorCallback;
import unpri.skripsi.iklan.io.ApiInterface;
import unpri.skripsi.iklan.io.BusProvider;
import unpri.skripsi.iklan.io.ErrorUtils;
import unpri.skripsi.iklan.io.RestPhp;

/**
 * Created by edwin on 4/23/2016.
 */
public class UtilityController extends BaseController{
    private static UtilityController _instance;
    private Context context;
    private String message="ERROR";
    private String sukses;
    private ProgressDialog dialog;
    protected UtilityController(Context paramContext)
    {
        super(paramContext);
        this.context = paramContext;
    }

    public static UtilityController getInstance(Context paramContext)
    {
        if (_instance == null) {
            _instance = new UtilityController(paramContext);
        }
        return _instance;
    }

    @Produce
    public void getDirectionRoute(Map<String, String>  parameters, final DirectionRouteGmapsInterface directionRouteGmapsInterface){
        ApiInterface service = RestPhp.getClient();
        Call<DirectionRouteGMapsCallback> call = service.getDirectionRoute(parameters);
        call.enqueue(new Callback<DirectionRouteGMapsCallback>() {
            @Override
            public void onResponse(Call<DirectionRouteGMapsCallback> call, Response<DirectionRouteGMapsCallback> response) {
                if (response.isSuccessful()) {
                    DirectionRouteGMapsCallback Data = response.body();
                    Data.setCallback("getDirectionRoute");
                    directionRouteGmapsInterface.onSuccessGetRoute(Data);
                } else {
                    APIErrorCallback error = ErrorUtils.parseError(response);
                    error.setCallback("getDirectionRoute");
                    directionRouteGmapsInterface.onErrorGetRoute(error);
                }
            }

            @Override
            public void onFailure(Call<DirectionRouteGMapsCallback> call, Throwable t) {
                APIErrorCallback error =new APIErrorCallback();
                error.setCallback("getDirectionRoute");
                String error_msg = t.getMessage();
                if(error_msg!=null) {
                    error.setError(error_msg+"");
                }
                else{
                    error.setError(context.getResources().getString(R.string.error));
                }
                directionRouteGmapsInterface.onErrorGetRoute(error);
            }
        });
    }

}

