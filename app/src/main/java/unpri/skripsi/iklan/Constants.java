package unpri.skripsi.iklan;

/**
 * Created by GleasonK on 6/8/15.
 *
 * Constants used by this chatting application.
 * TODO: Replace PUBLISH_KEY and SUBSCRIBE_KEY with your personal keys.
 * TODO: Register app for GCM and replace GCM_SENDER_ID
 */
public class Constants {

    public static final String DOORSMEER_DATA_PARCELABLE = "doorsmeers_item_data";
}
