package unpri.skripsi.iklan.dao;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import unpri.skripsi.iklan.models.IklanData;
import unpri.skripsi.iklan.utility.Utility;

/**
 * Created by Huang on 6/18/2017.
 */

public class DBController extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;
    private Context mContext;
    private SQLiteDatabase IklanDataBase;
    // Database Name
    private static final String DATABASE_NAME = "doorsmeerDB";
    private static String DB_PATH ;


    // Contacts table name
    private static final String TABLE_DOORSMEER_FAVORITE = "doorsmeer_favorite";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_TELP= "telp";
    private static final String KEY_ADDRESS= "address";
    private static final String KEY_COVER= "cover";
    private static final String KEY_DESCRIPTION= "description";
    private static final String KEY_LATITUDE= "latitude";
    private static final String KEY_LONGITUDE= "longitude";
    private String CREATE_FAVORITE_DOORSMEER_TABLE = "CREATE TABLE " + TABLE_DOORSMEER_FAVORITE + "("
            + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_NAME + " TEXT,"
            + KEY_TELP+ " TEXT,"
            + KEY_ADDRESS+ " TEXT,"
            + KEY_COVER+ " TEXT,"
            + KEY_DESCRIPTION+ " TEXT,"
            + KEY_LATITUDE+ " TEXT,"
            + KEY_LONGITUDE+ " TEXT" + ")";
    private String DELETE_FAVORITE_ORPHANAGE_TABLE = "DROP TABLE IF EXISTS " + TABLE_DOORSMEER_FAVORITE;
    private IklanData IklanData;

    public DBController(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        PackageManager pM = context.getPackageManager();
        String packageName = context.getPackageName();
        try {
            PackageInfo p = pM.getPackageInfo(packageName, 0);
            packageName = p.applicationInfo.dataDir;
            DB_PATH = packageName +"/databases/";
        } catch (PackageManager.NameNotFoundException e) {
            Log.w("yourtag", "Error Package name not found ", e);
        }
        this.mContext = context;
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_FAVORITE_DOORSMEER_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DOORSMEER_FAVORITE);

        // Create tables again
        onCreate(db);
    }
    public void openDataBase() throws SQLException {
        //Open the database
        String myPath = DB_PATH + DATABASE_NAME;
        File file = new File(myPath);
        if (file.exists() && !file.isDirectory())
            IklanDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY+ SQLiteDatabase.NO_LOCALIZED_COLLATORS);

    }

//    public long insertFavoriteDoorsmeer(IklanData IklanData) {
//        IklanDataBase = this.getReadableDatabase();
//        ContentValues values = new ContentValues();
//        values.put(KEY_ID, IklanData.id);
//        values.put(KEY_NAME, IklanData.doorsmeer_name);
//        values.put(KEY_TELP, IklanData.nohp);
//        values.put(KEY_ADDRESS, IklanData.alamat);
//        values.put(KEY_COVER, IklanData.coverImage);
//        values.put(KEY_DESCRIPTION, IklanData.description);
//        values.put(KEY_LATITUDE, IklanData.latitude);
//        values.put(KEY_LONGITUDE, IklanData.longitude);
//        long id =  IklanDataBase.insert(TABLE_DOORSMEER_FAVORITE, null, values);
//        IklanDataBase.close();
//        return id;
//    }
    public void deleteFavoriteDoorsmeer(String doorsmeer_id) {
        IklanDataBase = this.getReadableDatabase();
        IklanDataBase.delete(TABLE_DOORSMEER_FAVORITE, "`"+KEY_ID+"` = '"+ doorsmeer_id+"'", null);
        IklanDataBase.close();
    }
//    public ArrayList<IklanData> getFavoriteDoorsmeer() {
//        ArrayList<IklanData> IklanDatas= new ArrayList<IklanData>();
//        IklanDataBase = this.getReadableDatabase();
//        String selectQuery = "SELECT * FROM `"+TABLE_DOORSMEER_FAVORITE+"`";
//        Cursor cursor = IklanDataBase.rawQuery(selectQuery, null);
//        if (cursor.moveToFirst()) {
//            do {
//                IklanData IklanData= new IklanData();
//                IklanData.id = cursor.getString(cursor.getColumnIndex(KEY_ID));
//                IklanData.doorsmeer_name= cursor.getString(cursor.getColumnIndex(KEY_NAME));
//                IklanData.nohp= cursor.getString(cursor.getColumnIndex(KEY_TELP));
//                IklanData.alamat= cursor.getString(cursor.getColumnIndex(KEY_ADDRESS));
//                IklanData.coverImage= cursor.getString(cursor.getColumnIndex(KEY_COVER));
//                IklanData.description= cursor.getString(cursor.getColumnIndex(KEY_DESCRIPTION));
//                IklanData.latitude= Utility.getInstance().parseDecimal(cursor.getString(cursor.getColumnIndex(KEY_LATITUDE)));
//                IklanData.longitude= Utility.getInstance().parseDecimal(cursor.getString(cursor.getColumnIndex(KEY_LONGITUDE)));
//                IklanDatas.add(IklanData);
//            } while (cursor.moveToNext());
//        }
//        cursor.close();
//        IklanDataBase.close();
//        return IklanDatas;
//    }
    public void updateLocationImage(String chat_id, String location_image) {
        IklanDataBase = this.getReadableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("location_image", location_image); //These Fields should be your String values of actual column names
        IklanDataBase.update(TABLE_DOORSMEER_FAVORITE, cv, "`id`="+chat_id, null);
        IklanDataBase.close();
    }
}
