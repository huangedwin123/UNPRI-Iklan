package unpri.skripsi.iklan.utility;

/**
 * Created by Huang on 4/23/2017.
 */


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import unpri.skripsi.iklan.interfaceclass.OnGoogleConnectionListener;
import unpri.skripsi.iklan.interfaceclass.OnLocationChangeListener;

import static android.content.Context.LOCATION_SERVICE;

public class GPSTracker implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,LocationListener{
    public static final String CAUSE_NETWORK_LOST = "Network lost";
    public static final String CAUSE_SERVICE_DISCONNECTED = "Service disconnected";
    public static final String CAUSE_UNDEFINED = "Undefined";
    private static int EXPIRATION_DURATION = 40000;
    private final Context mContext;
    private GoogleApiClient googleApiClient;
    private OnLocationChangeListener onLocationChangeListener;

    private static int UPDATE_INTERVAL = 10000; // 10 sec
    private static int FATEST_INTERVAL = 5000; // 5 sec
    private static int DISPLACEMENT = 10; // 10 meters
    LocationRequest create;
    private OnGoogleConnectionListener onGoogleConnectionListener;


    public GPSTracker(Context context) {
        mContext = context;
        this.googleApiClient = new GoogleApiClient.Builder(context).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        createLocationRequest();
    }
    public void setOnGoogleConnectionListener(OnGoogleConnectionListener onGoogleConnectionListener){
        this.onGoogleConnectionListener = onGoogleConnectionListener;
    }
    private static GPSTracker _instance;
    public void setOnLocationChangeListener(OnLocationChangeListener onLocationChangeListener){
        this.onLocationChangeListener = onLocationChangeListener;
    }

    public static GPSTracker getInstance(Context context) {
        if (_instance == null) {
            _instance = new GPSTracker(context);
        }
        return _instance;
    }
    public void connect() {
        this.googleApiClient.connect();
    }

    public void disconnect() {
        if (this.googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(this.googleApiClient, this);
            this.googleApiClient.disconnect();
        }
    }

    public void onConnected(Bundle bundle) {
        GPSTracker.this.onGoogleConnectionListener.onGoogleConnected();
    }

    public void onConnectionSuspended(int i) {
        String str;
        if (i == 2) {
            str = CAUSE_NETWORK_LOST;
        } else if (i == 1) {
            str = CAUSE_SERVICE_DISCONNECTED;
        } else {
            str = CAUSE_UNDEFINED;
        }
        GPSTracker.this.onGoogleConnectionListener.onGoogleConnectionError(str);
    }
    // Request code to use when launching the resolution activity
    private static final int REQUEST_RESOLVE_ERROR = 1001;
    // Unique tag for the error dialog fragment
    private static final String DIALOG_ERROR = "dialog_error";
    // Bool to track whether the app is already resolving an error
    private boolean mResolvingError = false;
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "on failed ");
//        if (mResolvingError) {
//            // Already attempting to resolve an error.
//            return;
//        } else if (connectionResult.hasResolution()) {
//            try {
//                mResolvingError = true;
//                connectionResult.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
//            } catch (IntentSender.SendIntentException e) {
//                // There was an error with the resolution intent. Try again.
//                googleApiClient.connect();
//            }
//        } else {
//            // Show dialog using GoogleApiAvailability.getErrorDialog()
//            showErrorDialog(connectionResult.getErrorCode());
//            mResolvingError = true;
//        }
        GPSTracker.this.onGoogleConnectionListener.onGoogleConnectionError(connectionResult.getErrorMessage());
    }
    // The rest of this code is all about building the error dialog
    /* Creates a dialog for an error message */
//    private void showErrorDialog(int errorCode) {
//        // Create a fragment for the error dialog
//        ErrorDialogFragment dialogFragment = new ErrorDialogFragment();
//        // Pass the error that should be displayed
//        Bundle args = new Bundle();
//        args.putInt(DIALOG_ERROR, errorCode);
//        dialogFragment.setArguments(args);
//        dialogFragment.show(((Activity)mContext).getFragmentManager(), "errordialog");
//    }
//
//    /* Called from ErrorDialogFragment when the dialog is dismissed. */
//    public void onDialogDismissed() {
//        mResolvingError = false;
//    }
//
//    /* A fragment to display an error dialog */
//    public static class ErrorDialogFragment extends DialogFragment {
//        public ErrorDialogFragment() { }
//
//        @Override
//        public Dialog onCreateDialog(Bundle savedInstanceState) {
//            // Get the error code and retrieve the appropriate dialog
//            int errorCode = this.getArguments().getInt(DIALOG_ERROR);
//            return GoogleApiAvailability.getInstance().getErrorDialog(
//                    this.getActivity(), errorCode, REQUEST_RESOLVE_ERROR);
//        }
//
//        @Override
//        public void onDismiss(DialogInterface dialog) {
//            ((Activity) mContext).onDialogDismissed();
//        }
//    }

    public Location getGoogleAPILocation(){
        Location mLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        return mLocation;
    }
    public void requestLocation() {
        PendingResult<Status> locationUpdates= LocationServices.FusedLocationApi.requestLocationUpdates(this.googleApiClient, create, this);
//        locationUpdates.setResultCallback(new ResultCallback<Status>() {
//			@Override
//			public void onResult(@NonNull Status status) {
//
//			}
//		});
        Log.d("sf","saf");
    }
    private void createLocationRequest(){
        create = new LocationRequest();
        create.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        create.setInterval(FATEST_INTERVAL);
        create.setFastestInterval(UPDATE_INTERVAL);
        create.setSmallestDisplacement(DISPLACEMENT);
//        create.setExpirationDuration(EXPIRATION_DURATION);
    }
    public boolean isGoogleAPIConnected(){
        return googleApiClient.isConnected();
    }
    @Override
    public void onLocationChanged(Location location) {
        Log.i(GPSTracker.TAG, "on location changed");
        if(GPSTracker.this.onLocationChangeListener!=null) {
            GPSTracker.this.onLocationChangeListener.onLocationReceived(location);
        }
    }

    public boolean isLocationEnabled(Context mContext) {
        boolean isGPSEnabled = false;
        boolean isNetworkEnabled = false;
        LocationManager locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
        try {
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e.getCause());
        }
        try {
            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception e2) {
            Log.e(TAG, e2.getMessage(), e2.getCause());
        }
        return isGPSEnabled || isNetworkEnabled;
    }

    boolean isShowing = false;
    private static String TAG = null;
    AlertDialog aDialog;
    public void showSettingsAlert(final Context mContext){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        // Setting Dialog Title
        alertDialog.setTitle("Perhatian");

        // Setting Dialog Message
        alertDialog.setMessage("Silahkan hidupkan layanan GPS lokasi anda, dan setel ke mode akurasi tinggi");

        // On pressing Settings button
        alertDialog.setPositiveButton("Setelan", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                ((Activity) mContext).startActivity(intent);
                isShowing = false;
            }
        });

        // on pressing cancel button
        aDialog = alertDialog.create();
        aDialog.setCanceledOnTouchOutside(true);
        aDialog.setCancelable(false);
        // Showing Alert Message
        try {
            aDialog.show();
            isShowing = true;
        } catch(Exception e){
            // WindowManager$BadTokenException will be caught and the app would not display
            // the 'Force Close' message
            Log.e("EXCEPTION","LOGGED");
        }
    }

    public void showGPSHighAccuracyAlert(final Context mContext){
        //require param Context because context is not same between activities in use singleton.. dialog will not show

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        // Setting Dialog Title
        alertDialog.setTitle("Perhatian");

        // Setting Dialog Message
        alertDialog.setMessage("Pastikan anda sudah ubah gps ke mode akurasi tinggi. silakan mencoba kembali");

        // On pressing Settings button
        alertDialog.setPositiveButton("Setelan", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                dialog.dismiss();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                Activity activity = (Activity) mContext;
                activity.startActivityForResult(intent, 1);
                isShowing = false;
            }
        });
        // on pressing cancel button
        aDialog= alertDialog.create();
        aDialog.setCanceledOnTouchOutside(true);
        aDialog.setCancelable(false);
        // Showing Alert Message
        try {
            aDialog.show();
            isShowing = true;
        } catch(Exception e){
            // WindowManager$BadTokenException will be caught and the app would not display
            // the 'Force Close' message
            Log.e("EXCEPTION","LOGGED");
        }
    }
    public boolean isAlertDialogShowing(){
        return isShowing;
    }
    public AlertDialog getAlertDialog(){
        return aDialog;
    }

}
