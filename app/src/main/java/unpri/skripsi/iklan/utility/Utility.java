package unpri.skripsi.iklan.utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import unpri.skripsi.iklan.R;

/**
 * Created by Huang on 4/23/2017.
 */

public class Utility {
    private static Utility instance = null;
    private boolean forceUpdated = false;

    public static Utility getInstance() {
        if (instance == null) {
            instance = new Utility();
        }
        return instance;
    }

    public int parseInteger(String valueString) {
        int valueInt = 0;
        try {
            try {
                double valueDouble = Double.parseDouble(valueString);
                valueInt = (int) valueDouble;
            } catch (NullPointerException ne) {
            }
        } catch (NumberFormatException e) {
        }
        return valueInt;
    }

    public double parseDecimal(String valueString) {
        double valueDouble = 0.0;
        try {
            valueDouble = Double.parseDouble(valueString);

        } catch (Exception localException) {
        }
        return valueDouble;
    }

    public int getDisplayHeight(Context context) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        Activity activity = (Activity) context;
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int screenHeight = displaymetrics.heightPixels;
        return screenHeight;
    }

    public int getDisplayWidth(Context context) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        Activity activity = (Activity) context;
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int screenWidth = displaymetrics.widthPixels;
        return screenWidth;
    }

    public LinearLayout.LayoutParams getFoodRestaurantItemHeight(Context context) {
        Activity activity = (Activity) context;
        DisplayMetrics displaymetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        float screenWidth = displaymetrics.widthPixels;
        float screenHeight = displaymetrics.heightPixels;
        double imageHeight = screenHeight / 3.0;
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) imageHeight); //WRAP_CONTENT param can be FILL_PARENT
//        Float density = context.getResources().getDisplayMetrics().density;
//        if (density <= 0.75) { // LDPI
//            leftMarginWidth = screenWidth * 0.7;
//            topMarginHeight = screenHeight * 0.28;
//        } else if (density <= 1.0) { // MDPI
//            leftMarginWidth = screenWidth * 0.7;
//            topMarginHeight = screenHeight * 0.28;
//        } else if (density <= 1.5) { // HDPI
//            leftMarginWidth = screenWidth * 0.7;
//            topMarginHeight = screenHeight * 0.3;
//        } else if (density <= 2.0) { // XHDPI
//            leftMarginWidth = screenWidth * 0.7;
//            topMarginHeight = screenHeight * 0.32;
//        } else if (density <= 3.0) { // XXHDPI
//            leftMarginWidth = screenWidth * 0.7;
//            topMarginHeight = screenHeight * 0.31;
//        } else if (density <= 4.0) { // XXXHDPI
//            leftMarginWidth = screenWidth * 0.7;
//            topMarginHeight = screenHeight * 0.31;
//        }
        return params;
    }


    public boolean isEmulator() {
        return Build.FINGERPRINT.startsWith("generic")
                || Build.FINGERPRINT.startsWith("unknown")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.MANUFACTURER.contains("Genymotion")
                || (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
                || "google_sdk".equals(Build.PRODUCT);
    }

    public static String getAppVersionName(Context paramContext) {
        try {
            String str = paramContext.getPackageManager().getPackageInfo(paramContext.getPackageName(), 0).versionName;
            return str;
        } catch (PackageManager.NameNotFoundException localNameNotFoundException) {
            localNameNotFoundException.printStackTrace();
        }
        return "";
    }

    public int getAppVersionCode(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    public void intentActivity(Activity activity, String StringClassname) {
        Class<?> className = null;
        if (StringClassname != null) {
            StringClassname = "com.pmberjaya.indotiki.app." + StringClassname;
            try {
                className = Class.forName(StringClassname);
                Intent intent = new Intent(activity, className);
                activity.startActivity(intent);
            } catch (ClassNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public static String formatTimeStamp(long timeStamp, String formatTime) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(formatTime);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeStamp);
        return formatter.format(calendar.getTime());
    }

    public String uppercaseText(String str) {
        String[] strArray = str.split(" ");
        StringBuilder builder = new StringBuilder();
        for (String s : strArray) {
            String cap = s.substring(0, 1).toUpperCase() + s.substring(1);
            builder.append(cap + " ");
        }
        return builder.toString();
    }

    public String getAppTypeVersion(int type) {
        if (type == 0) {
            return "LIVE";
        } else {
            return "BETA";
        }
    }


    public void showSimpleAlertDialog(Context context, String title, String msg, String positiveMessage, DialogInterface.OnClickListener positiveListener, String negativeMessage, DialogInterface.OnClickListener negativeListener) {
        AlertDialog dialog;
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (title != null) {
            builder.setTitle(title);
        }
        builder.setMessage(msg)
                .setCancelable(false)
                .setIcon(R.mipmap.ic_launcher);
        if (positiveMessage != null) {
            builder.setPositiveButton(positiveMessage, positiveListener);
        }
        if (negativeMessage != null) {
            builder.setNegativeButton(negativeMessage, negativeListener);
        }
        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }


    public boolean isValidUntilNow(String timeData) {
        Calendar secondsTime = Calendar.getInstance();
        int secondsTimes = Integer.parseInt(timeData);
        secondsTime.setTimeInMillis((long) secondsTimes * 1000);

        int day = secondsTime.get(Calendar.DAY_OF_MONTH);
        int month = secondsTime.get(Calendar.MONTH) + 1;
        int year = secondsTime.get(Calendar.YEAR);
        if (day >= 12 && month >= 4 && year >= 2017) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isOpened(String openTime, String closeTime) {
        boolean isOpened = false;
        if (openTime == null || closeTime == null) {
            isOpened = false;
        } else {
            Calendar c = Calendar.getInstance();
            int hour_of_day = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);
            String[] openTimeArray = openTime.split(":");
            String[] closeTimeArray = closeTime.split(":");
            int openTimeHour = Integer.parseInt(openTimeArray[0]);
            int openTimeMinute = Integer.parseInt(openTimeArray[1]);
            int closeTimeHour = Integer.parseInt(closeTimeArray[0]);
            int closeTimeMinute = Integer.parseInt(closeTimeArray[1]);
            if (openTimeHour > closeTimeHour) {
                if ((hour_of_day > openTimeHour && hour_of_day < 23) || hour_of_day < closeTimeHour) {
                    isOpened = true;
                } else if (hour_of_day == openTimeHour || hour_of_day == closeTimeHour) {
                    if (hour_of_day == openTimeHour) {
                        if (minute >= openTimeMinute && minute <= 59) {
                            isOpened = true;
                        } else {
                            isOpened = false;
                        }
                    } else if (hour_of_day == closeTimeHour) {
                        if (minute >= 0 && minute <= closeTimeMinute) {
                            isOpened = true;
                        } else {
                            isOpened = false;
                        }
                    }
                } else {
                    isOpened = false;
                }
            } else if (openTimeHour < closeTimeHour) {
                if (hour_of_day > openTimeHour && hour_of_day < closeTimeHour) {
                    isOpened = true;
                } else if (hour_of_day == openTimeHour || hour_of_day == closeTimeHour) {
                    if (hour_of_day == openTimeHour) {
                        if (minute >= openTimeMinute && minute <= 59) {
                            isOpened = true;
                        } else {
                            isOpened = false;
                        }
                    } else if (hour_of_day == closeTimeHour) {
                        if (minute >= 0 && minute <= closeTimeMinute) {
                            isOpened = true;
                        } else {
                            isOpened = false;
                        }
                    }
                } else {
                    isOpened = false;
                }
            }

        }
        return isOpened;
    }

    public boolean isTextNotNullEmpty(String text) {
        if (text == null || TextUtils.isEmpty(text)) {
            return false;
        }
        return true;
    }

    public boolean isInternetOn(Context paramContext) {
        NetworkInfo localNetworkInfo = ((ConnectivityManager) paramContext.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if ((localNetworkInfo == null) || (!localNetworkInfo.isAvailable())) {
            return false;
        } else {
            return true;
        }
    }

    public long getTimeStamp(String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = sdf.parse(date);

        Calendar c = Calendar.getInstance();
        c.setTime(d);
        long time = c.getTimeInMillis();
        long curr = System.currentTimeMillis();
        long diff = time;    //Time difference in milliseconds
        return diff / 1000;
    }



  /*public static void deleteCache(Context paramContext)
  {
    try
    {
      File localFile = paramContext.getCacheDir();
      if ((localFile != null) && (localFile.isDirectory())) {
        deleteDir(localFile);
      }
      return;
    }
    catch (Exception localException) {}
  }

  private static boolean deleteDir(File paramFile)
  {
	  return status;
  }*/

 /* public static int getColor(Resources paramResources, int paramInt, Resources.Theme paramTheme)
    throws Resources.NotFoundException
  {
    if (Build.VERSION.SDK_INT >= 23) {
      return paramResources.getColor(paramInt, paramTheme);
    }
    return paramResources.getColor(paramInt);
  }

  public static String getDeviceUID(Context paramContext)
  {
    return Settings.Secure.getString(paramContext.getContentResolver(), "android_id");
  }

  public static String getEllipsizeString(String paramString, int paramInt1, int paramInt2)
  {
    String str = "";
    if ((paramString == null) || (paramString.length() == 0)) {
      return str;
    }
    int i;
    if (paramInt2 > 0)
    {
      i = -1;
      for (int j = 0; j < paramInt2 - 1; j++) {
        if ((paramString.contains("\n")) || (paramString.contains("<br>")))
        {
          i = paramString.indexOf("<br>", i + 4);
          if (i == -1) {
            i = paramString.indexOf('\n', i + 1);
          }
        }
      }
      if (i != -1) {
        break label133;
      }
    }
    label133:
    for (str = paramString;; str = paramString.substring(0, i) + "...")
    {
      if (str.length() > paramInt1) {
        str = str.substring(0, paramInt1 - 3) + "...";
      }
      return str;
    }
  }*/



  /*public static boolean isAppNewer(Context paramContext)
  {
    try
    {
      int i = paramContext.getPackageManager().getPackageInfo(paramContext.getPackageName(), 0).versionCode;
      int j = PreferenceUtility.getInstance().loadDataInt(paramContext, "version_code");
      Clog.d("=== Current Version Code : " + i);
      Clog.d("=== Preference Version Code : " + j);
      boolean bool = false;
      if (i > j)
      {
        PreferenceUtility.getInstance().saveData(paramContext, "version_code", i);
        bool = true;
      }
      return bool;
    }
    catch (Exception localException)
    {
      Clog.e("=== Error : " + localException.getMessage());
    }
    return false;
  }*/






 /* public static void setInstance(Utility paramUtility)
  {
    try
    {
      instance = paramUtility;
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }*/

    public double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    public String convertPrice(double paramString) {
        try {
            DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.GERMAN);
            otherSymbols.setDecimalSeparator(',');
            otherSymbols.setGroupingSeparator('.');
            DecimalFormat dfnd = new DecimalFormat("#,###", otherSymbols);

            String a = dfnd.format(paramString);
            return a;
        } catch (Exception localException) {
            localException.printStackTrace();
        }
        return "0";
    }

    public String convertPrice(String paramString) {
        try {
            BigDecimal localBigDecimal = new BigDecimal(paramString);
            DecimalFormatSymbols localDecimalFormatSymbols = DecimalFormatSymbols.getInstance();
            localDecimalFormatSymbols.setGroupingSeparator('.');
            return new DecimalFormat("###,###.##", localDecimalFormatSymbols).format(localBigDecimal.longValue());
        } catch (Exception localException) {
            localException.printStackTrace();
        }
        return "0";
    }

    public String convertint(String paramString) {
        try {
            BigDecimal localBigDecimal = new BigDecimal(paramString);
            DecimalFormatSymbols localDecimalFormatSymbols = DecimalFormatSymbols.getInstance();
            localDecimalFormatSymbols.setGroupingSeparator('.');
            return new DecimalFormat("########", localDecimalFormatSymbols).format(localBigDecimal.longValue());
        } catch (Exception localException) {
            localException.printStackTrace();
        }
        return "0";
    }

    public String shuffle() {
        String input = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        List<Character> characters = new ArrayList<Character>();
        for (char c : input.toCharArray()) {
            characters.add(c);
        }
        StringBuilder output = new StringBuilder(input.length());
        while (characters.size() != 0) {
            int randPicker = (int) (Math.random() * characters.size());
            output.append(characters.remove(randPicker));
        }
        return "inki_" + output.toString().substring(0, 8);
    }

    public String formatDate(String paramString) {
        try {
            SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", new Locale("in"));
            String str = localSimpleDateFormat.format(localSimpleDateFormat.parse(paramString));
            return str;
        } catch (Exception localException) {
        }
        return paramString;
    }

/*  public String getAccessToken(Context paramContext)
  {
    String str = PreferenceUtility.getInstance().loadDataString(paramContext, "user_data");
    if (str != null)
    {
      Login localLogin = (Login)new Gson().fromJson(str, Login.class);
      if (localLogin != null) {
        return localLogin.getAccessToken();
      }
    }
    return "";
  }*/



/*  public boolean isFirstLaunch(Context paramContext)
  {
    boolean bool = PreferenceUtility.getInstance().loadDataBoolean(paramContext, "first_launch").booleanValue();
    if (bool) {
      PreferenceUtility.getInstance().saveData(paramContext, "first_launch", Boolean.valueOf(false));
    }
    return bool;
  }*/


    public void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            float px = 300 * (listView.getResources().getDisplayMetrics().density);
            listItem.measure(View.MeasureSpec.makeMeasureSpec((int) px, View.MeasureSpec.AT_MOST), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static int getColor(Resources paramResources, int paramInt, Resources.Theme paramTheme)
            throws Resources.NotFoundException {
        if (Build.VERSION.SDK_INT >= 23) {
            return paramResources.getColor(paramInt, paramTheme);
        }
        return paramResources.getColor(paramInt);
    }

    public String phoneFormat(String phone) {
        if (phone.substring(0, 1) != null && phone.substring(0, 1).equals("0")) {
            phone = phone.substring(0, 1).replace("0", "+62") + phone.substring(1, phone.length());
        } else if (phone.substring(0, 2) != null && phone.substring(0, 2).equals("62")) {
            phone = phone.substring(0, 2).replace("62", "+62") + phone.substring(2, phone.length());
        }
        return phone;
    }

    public String phoneFormatWithCountryCode(String phone) {
        if (phone.substring(0, 1) != null && phone.substring(0, 1).equals("0")) {
            phone = "+62" + phone.substring(0, 1).replace("0", "") + phone.substring(1, phone.length());
        } else if (phone.substring(0, 2) != null && phone.substring(0, 2).equals("62")) {
            phone = "+62" + phone.substring(0, 2).replace("62", "") + phone.substring(2, phone.length());
        } else {
            phone = "+62" + phone;
        }
        return phone;
    }

    public void makeAlertDialog(Context context, String text) {

        AlertDialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(null);
        builder.setMessage(text);
        builder.setIcon(R.mipmap.ic_launcher);
        // Set the action buttons

        builder.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        dialog = builder.create();
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);

    }

    public boolean checkIfStringIsNotNullOrEmpty(String text) {
        if (text != null && text.trim().length() != 0) {
            return true;
        } else {
            return false;
        }
    }
//    public void showAppNotification(Context context) {
//        // In this sample, we'll use the same text for the ticker and the expanded notification
//        CharSequence text = context.getText(R.string.local_service_started);
//        Intent intent = new Intent(context, MainMenu.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        // The PendingIntent to launch our activity if the user selects this notification
//        PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
//                intent, 0);
//        NotificationManager mNotificationManager = (NotificationManager)
//                context.getSystemService(Context.NOTIFICATION_SERVICE);
//        // Set the info for the views that show in the notification panel.
//        Notification notification = new Notification.Builder(context)
//                .setSmallIcon(R.drawable.ic_launcher)  // the status icon
//                .setTicker(text)  // the status text
//                .setWhen(System.currentTimeMillis())  // the time stamp
//                .setContentTitle(context.getText(R.string.local_service_label))  // the label of the entry
//                .setContentText(text)  // the contents of the entry
//                .setContentIntent(contentIntent)  // The intent to send when the entry is clicked
//                .build();
//
//        // Send the notification.
//        notification.flags = Notification.FLAG_ONGOING_EVENT;
//        mNotificationManager.notify(0, notification);  // Set notification ID
//
//    }
}