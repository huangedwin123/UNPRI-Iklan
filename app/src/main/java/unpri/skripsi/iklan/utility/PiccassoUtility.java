package unpri.skripsi.iklan.utility;

import android.content.Context;
import android.os.Environment;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

import unpri.skripsi.iklan.R;

/**
 * Created by Huang on 6/11/2017.
 */

public class PiccassoUtility {
    public static void loadImage(Context context, String url, ImageView image){
        if(url!=null && !url.equals("")) {
            Picasso.with(context).load(url).error(R.drawable.img_nophoto).noFade().into(image);
        }else{
            loadImageFail(context, image);
        }
    }

    public static void loadImageFail(Context context, ImageView image){
        Picasso.with(context).load(R.drawable.img_nophoto).error(R.drawable.img_nophoto).into(image);
    }

    public static String saveImageToFile(Context context, String sourceFile, long timestamp, String request_id, String request_type) throws IOException {
        String image_ext = ".JPG";
        final String imageName = "IMG-"+Utility.getInstance().formatTimeStamp(timestamp,"yyyyMMdd")+"-"+Utility.getInstance().formatTimeStamp(timestamp,"HHmmSS")+image_ext;
        String imageDir = Environment.getExternalStorageDirectory().getAbsolutePath() +"/"+context.getResources().getString(R.string.app_name)+"/"+request_type+"/"+request_id+"/"+"Sent/";

        File savePhoto = new File(imageDir);
        if(!savePhoto.exists()){
            savePhoto.mkdirs();
        }
        File source = new File (sourceFile);
        String filePath = imageDir+imageName;
        File output = new File(filePath);
        try
        {
            FileUtils.copyFile(source, output);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return filePath;
    }
    static boolean isSuccessLoad;

    public static boolean loadImageFromDir(Context context, String image_path, ImageView ivImage){
        File myImageFile = new File(image_path);

        Picasso.with(context).load(myImageFile).into(ivImage, new Callback() {
            @Override
            public void onSuccess() {
                isSuccessLoad = true;
            }
            @Override
            public void onError() {
                isSuccessLoad = false;
            }

        });
        return isSuccessLoad;
    }
    public static void deleteImageFromDir(Context context, String request_type, String request_id) {
        File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() +"/"+context.getResources().getString(R.string.app_name)+"/"+request_type+"/"+request_id+"/");
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                new File(dir, children[i]).delete();
            }
        }
    }
}
