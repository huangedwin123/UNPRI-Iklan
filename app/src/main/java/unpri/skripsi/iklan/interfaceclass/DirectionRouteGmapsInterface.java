package unpri.skripsi.iklan.interfaceclass;

import unpri.skripsi.iklan.callbacks.DirectionRouteGMapsCallback;
import unpri.skripsi.iklan.io.APIErrorCallback;

/**
 * Created by Huang on 8/28/2017.
 */

public interface DirectionRouteGmapsInterface {
    void onSuccessGetRoute(DirectionRouteGMapsCallback directionRouteGMapsCallback);
    void onErrorGetRoute(APIErrorCallback apiErrorCallback);
}
