package unpri.skripsi.iklan.interfaceclass;

import unpri.skripsi.iklan.callbacks.SearchDoorsmeerCallback;
import unpri.skripsi.iklan.io.APIErrorCallback;

/**
 * Created by edwin on 20/07/2017.
 */

public interface SearchDoorsmeerInterface {
    void OnSuccessSearchDoorsmeer(SearchDoorsmeerCallback searchDoorsmeerCallback);
    void OnErrorSearchDoorsmeer(APIErrorCallback apiErrorCallback);
}
