package unpri.skripsi.iklan.interfaceclass;

import unpri.skripsi.iklan.callbacks.AdsTipeListCallback;
import unpri.skripsi.iklan.callbacks.DoorsmeerDetailCallback;
import unpri.skripsi.iklan.io.APIErrorCallback;

/**
 * Created by Huang on 8/1/2017.
 */

public interface AdsTipeInterface {
    void onSuccessAdsTipe(AdsTipeListCallback doorsmeerDetailCallback);
    void onErrorAdsTipe(APIErrorCallback apiErrorCallback);
}
