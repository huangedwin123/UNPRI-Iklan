package unpri.skripsi.iklan.interfaceclass;

import unpri.skripsi.iklan.callbacks.DoorsmeerDetailCallback;
import unpri.skripsi.iklan.io.APIErrorCallback;

/**
 * Created by Huang on 7/31/2017.
 */

public interface DoorsmeerDetailInterface {
    void onSuccessGetDoorsmeerDetail(DoorsmeerDetailCallback doorsmeerDetailCallback);
    void onErrorGetDoorsmeerDetail(APIErrorCallback apiErrorCallback);
}
