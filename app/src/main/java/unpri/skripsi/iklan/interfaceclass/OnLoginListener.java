package unpri.skripsi.iklan.interfaceclass;

import unpri.skripsi.iklan.callbacks.LoginCallback;
import unpri.skripsi.iklan.io.APIErrorCallback;

/**
 * Created by Huang on 7/19/2017.
 */

public interface OnLoginListener{
    void onLoginSuccess(LoginCallback loginCallback);
    void onLoginError(APIErrorCallback apiErrorCallback);
}
