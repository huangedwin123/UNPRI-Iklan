package unpri.skripsi.iklan.interfaceclass;

import unpri.skripsi.iklan.callbacks.DoorsmeerCallback;
import unpri.skripsi.iklan.io.APIErrorCallback;

/**
 * Created by Huang on 7/21/2017.
 */

public interface DoorsmeerMapInterface {
    void onSuccessGetDoorsmeerMap(DoorsmeerCallback doorsmeerCallback);
    void onErrorGetDoorsmeerMap(APIErrorCallback apiErrorCallback);
}
