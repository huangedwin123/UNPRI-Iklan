package unpri.skripsi.iklan.interfaceclass;

/**
 * Created by edwin on 17/02/2017.
 */

public interface OnGoogleConnectionListener {
    void onGoogleConnected();
    void onGoogleConnectionError(String errorMsg);
}
