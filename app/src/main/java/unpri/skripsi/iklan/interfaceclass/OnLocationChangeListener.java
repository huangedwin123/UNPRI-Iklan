package unpri.skripsi.iklan.interfaceclass;

import android.location.Location;

/**
 * Created by edwin on 17/02/2017.
 */

public interface OnLocationChangeListener {
    void onLocationError(String str);

    void onLocationReceived(Location location);
}
