package unpri.skripsi.iklan.io;


/**
 * Created by edwin on 24/06/2016.
 */
public class APIErrorCallback {

    private boolean status;

    private String error;


    private String callback;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
    public String getCallback() {
        return callback;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }


}

