package unpri.skripsi.iklan.io;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

public class BusProvider
{
  private static final Bus BUS = new Bus(ThreadEnforcer.MAIN);
  
  public static Bus getInstance()
  {
    return BUS;
  }
}


/* Location:              D:\Decompile\classes_dex2jar.jar!\com\rumah123\io\BusProvider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */