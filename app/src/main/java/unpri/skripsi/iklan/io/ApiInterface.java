package unpri.skripsi.iklan.io;

import java.util.ArrayList;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import unpri.skripsi.iklan.callbacks.AdsTipeListCallback;
import unpri.skripsi.iklan.callbacks.DirectionRouteGMapsCallback;
import unpri.skripsi.iklan.callbacks.DoorsmeerCallback;
import unpri.skripsi.iklan.callbacks.DoorsmeerDetailCallback;
import unpri.skripsi.iklan.callbacks.EditProfilCallback;
import unpri.skripsi.iklan.callbacks.GetUserDataCallback;
import unpri.skripsi.iklan.callbacks.LoginCallback;
import unpri.skripsi.iklan.callbacks.RegisterCallback;
import unpri.skripsi.iklan.callbacks.SearchDoorsmeerCallback;
import unpri.skripsi.iklan.interfaceclass.DoorsmeerDetailInterface;
import unpri.skripsi.iklan.models.GetUserData;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("API/main/epulsa/action/list_product")
    Call<LoginCallback> postLogin(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API/main/epulsa/action/list_product")
    Call<RegisterCallback> postRegister(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API/main/epulsa/action/list_product")
    Call<EditProfilCallback> postEditProfil(@FieldMap Map<String, String> params);

    @GET("API/main/epulsa/action/list_product")
    Call<GetUserDataCallback> getUserData(String user_id);

    @GET("iklan/search_ads.php")
    Call<SearchDoorsmeerCallback> searchDoorsmeer(@QueryMap Map<String, String> params);

    @GET("iklan/ads_list.php")
    Call<DoorsmeerCallback> getIklanData();

    @GET("iklan/ads_detail.php")
    Call<DoorsmeerDetailCallback> getAdsDetail(@QueryMap Map<String, String> params);

    @GET("iklan/ads_tipe_list.php")
    Call<AdsTipeListCallback> getAdsTipeList(@QueryMap Map<String, String> params);

    @GET("https://maps.googleapis.com/maps/api/directions/json")
    Call<DirectionRouteGMapsCallback> getDirectionRoute(@QueryMap Map<String, String> params);

}