package unpri.skripsi.iklan.models.DirectionRouteGmapsData;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by edwin on 5/28/2016.
 */
public class Routes implements Parcelable{
    @SerializedName("legs")
    @Expose
    private List<Legs> legs = new ArrayList<Legs>();

    protected Routes(Parcel in) {
        legs = new ArrayList<Legs>();
        in.readList(legs,Legs.class.getClassLoader());
    }

    public static final Creator<Routes> CREATOR = new Creator<Routes>() {
        @Override
        public Routes createFromParcel(Parcel in) {
            return new Routes(in);
        }

        @Override
        public Routes[] newArray(int size) {
            return new Routes[size];
        }
    };

    /**
     * @return The elements
     */
    public List<Legs> getLegs() {
        return legs;
    }

    /**
     * @param legs The elements
     */
    public void setLegs(List<Legs> legs) {
        this.legs = legs;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(legs);
    }
}
