package unpri.skripsi.iklan.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Huang on 5/28/2017.
 */

public class GetUserData  implements Parcelable {
    String user_id;
    String fullname;
    String email;
    String avatar;
    String phone;
    int userlevel;

    public GetUserData(String fullname, String email, String avatar, String phone, String hide_phone) {
        this.fullname =fullname;
        this.email = email;
        this.avatar = avatar;
        this.phone = phone;
    }
    protected GetUserData(Parcel in) {
        this.user_id = in.readString();
        this.fullname = in.readString();
        this.email = in.readString();
        this.avatar = in.readString();
        this.phone = in.readString();
        this.userlevel = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(user_id);
        dest.writeString(fullname);
        dest.writeString(email);
        dest.writeString(avatar);
        dest.writeString(phone);
        dest.writeInt(userlevel);
    }

    public static final Parcelable.Creator<GetUserData> CREATOR = new Parcelable.Creator<GetUserData>() {
        @Override
        public GetUserData createFromParcel(Parcel in) {
            return new GetUserData(in);
        }

        @Override
        public GetUserData[] newArray(int size) {
            return new GetUserData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getUserlevel() {
        return userlevel;
    }

    public void setUserlevel(int userlevel) {
        this.userlevel = userlevel;
    }
}
