package unpri.skripsi.iklan.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Huang on 7/31/2017.
 */

public class IklanDetailData implements Parcelable{
    public String id;
    public String alamat;
    public String nama_jenis;
    public ArrayList<String> image;
    public String cover_image;
    public String deskripsi_iklan;
    public double latitude;
    public double longitude;

    public String nama_perusahaan;
    public String nomor_telepon;
    public String alamat_perusahaan;
    public String email;
    public String deskripsi;
    public String logo_perusahaan;


    protected IklanDetailData(Parcel in) {
        id = in.readString();
        alamat = in.readString();
        nama_jenis = in.readString();
        image = in.createStringArrayList();
        cover_image= in.readString();
        deskripsi_iklan = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        nama_perusahaan = in.readString();
        nomor_telepon = in.readString();
        alamat_perusahaan = in.readString();
        email = in.readString();
        deskripsi = in.readString();
        logo_perusahaan = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(alamat);
        dest.writeString(nama_jenis);
        dest.writeStringList(image);
        dest.writeString(cover_image);
        dest.writeString(deskripsi_iklan);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(nama_perusahaan);
        dest.writeString(nomor_telepon);
        dest.writeString(alamat_perusahaan);
        dest.writeString(email);
        dest.writeString(deskripsi);
        dest.writeString(logo_perusahaan);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<IklanDetailData> CREATOR = new Creator<IklanDetailData>() {
        @Override
        public IklanDetailData createFromParcel(Parcel in) {
            return new IklanDetailData(in);
        }

        @Override
        public IklanDetailData[] newArray(int size) {
            return new IklanDetailData[size];
        }
    };
}
