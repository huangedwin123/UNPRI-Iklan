package unpri.skripsi.iklan.models.DirectionRouteGmapsData;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by edwin on 5/28/2016.
 */
public class Legs implements Parcelable {
    @SerializedName("steps")
    @Expose
    private List<Steps> steps = new ArrayList<Steps>();
    @SerializedName("distance")
    @Expose
    private Distance distance;
    @SerializedName("duration")
    @Expose
    private Duration duration;

    protected Legs(Parcel in) {
        distance = in.readParcelable(Distance.class.getClassLoader());
        duration = in.readParcelable(Duration.class.getClassLoader());
        steps = new ArrayList<Steps>();
        in.readList(steps,Steps.class.getClassLoader());
    }

    public static final Creator<Legs> CREATOR = new Creator<Legs>() {
        @Override
        public Legs createFromParcel(Parcel in) {
            return new Legs(in);
        }

        @Override
        public Legs[] newArray(int size) {
            return new Legs[size];
        }
    };

    /**
     * @return The elements
     */
    public List<Steps> getSteps() {
        return steps;
    }

    /**
     * @param steps The elements
     */
    public void setSteps(List<Steps> steps) {
        this.steps = steps;
    }

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(distance, flags);
        dest.writeParcelable(duration, flags);
        dest.writeList(this.steps);
    }
}
