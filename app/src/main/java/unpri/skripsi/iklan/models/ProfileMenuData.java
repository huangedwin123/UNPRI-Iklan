package unpri.skripsi.iklan.models;

/**
 * Created by edwin on 19/06/2017.
 */

public class ProfileMenuData {
    int menuIcon;
    String menuName;

    public int getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(int menuIcon) {
        this.menuIcon = menuIcon;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }
}
