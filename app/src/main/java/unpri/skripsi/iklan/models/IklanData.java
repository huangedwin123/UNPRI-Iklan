package unpri.skripsi.iklan.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Huang on 4/23/2017.
 */

public class IklanData implements Parcelable{
    public String id;
    public String alamat;
    public String nama_perusahaan;
    public String nama_jenis;
    public ArrayList<String> image;
    public String cover_image;
    public String deskripsi_iklan;
    public double latitude;
    public double longitude;
    public String markerId;
    public IklanData(){
    }
    protected IklanData(Parcel in) {
        id = in.readString();
        alamat = in.readString();
        nama_perusahaan = in.readString();
        nama_jenis = in.readString();
        image = in.createStringArrayList();
        cover_image = in.readString();
        deskripsi_iklan = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        markerId = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(alamat);
        dest.writeString(nama_perusahaan);
        dest.writeString(nama_jenis);
        dest.writeStringList(image);
        dest.writeString(cover_image);
        dest.writeString(deskripsi_iklan);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(markerId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<IklanData> CREATOR = new Creator<IklanData>() {
        @Override
        public IklanData createFromParcel(Parcel in) {
            return new IklanData(in);
        }

        @Override
        public IklanData[] newArray(int size) {
            return new IklanData[size];
        }
    };
}
