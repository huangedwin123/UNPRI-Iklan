package unpri.skripsi.iklan.models.DirectionRouteGmapsData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by edwin on 5/28/2016.
 */
public class PolylineData {
    @SerializedName("points")
    @Expose
    private String points;

    /**
     * @return The points
     */
    public String getPoints() {
        return points;
    }

    /**
     * @param points
     */
    public void setPoints(String points) {
        this.points = points;
    }
}
