package unpri.skripsi.iklan.activities.Account;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.squareup.otto.Subscribe;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import unpri.skripsi.iklan.R;
import unpri.skripsi.iklan.activities.MainMenuTab;
import unpri.skripsi.iklan.callbacks.LoginCallback;
import unpri.skripsi.iklan.controllers.UserController;
import unpri.skripsi.iklan.interfaceclass.OnLoginListener;
import unpri.skripsi.iklan.io.APIErrorCallback;
import unpri.skripsi.iklan.io.BusProvider;
import unpri.skripsi.iklan.models.GetUserData;
import unpri.skripsi.iklan.utility.SessionManager;
import unpri.skripsi.iklan.utility.Utility;

/**
 * Created by edwin on 13/01/2017.
 */

public class Login extends AppCompatActivity implements OnLoginListener{
    private int logoHeight;
    private String userId;
    private ProgressDialog pDialog;
    private String personFirstName;
    private String personLastName;
    private String email_google;
    private String personFullName;
    private String url_photo;
    private String gplus_id;
    private String fullname;
    private String phone;
    private String avatar;
    private AlertDialog aDialog;
    //------------------------------------------------------    FACEBOOK------------------------------------
    private String email;
    String emailData;
    private String url_profile;
    private EditText emaillogin;
    private EditText passlogin;
    LinearLayout login_email_layout;
    //    Animation loginManualAnimFadeIn;
//    Animation loginManualAnimFadeOut;
    private RadioGroup rg_login_with;
    private UserController userController;
    private CheckBox showpass;
    private Button loginbutton;
    private LinearLayout daftarlayout;
    private boolean runDoLoginManual = false;
//    RadioButton fab_login_manual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userController = UserController.getInstance(Login.this);
//        setLayoutFullScreen();
        setContentView(R.layout.login);
        renderViews();
        init();
    }

    private void setLayoutFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void renderViews() {
        emaillogin = (EditText) findViewById(R.id.emaillogin);
        passlogin = (EditText) findViewById(R.id.passlogin);
        showpass = (CheckBox) findViewById (R.id.showpass);
        loginbutton = (Button) findViewById(R.id.loginbutton);
        daftarlayout = (LinearLayout) findViewById(R.id.daftarlayout);
    }

    private void init(){
        loginbutton.setOnClickListener(loginListener);
        daftarlayout.setOnClickListener(intentToRegister);
        showpass.setOnCheckedChangeListener(showPasswordListener);
    }

    public CompoundButton.OnCheckedChangeListener showPasswordListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(!isChecked) {
                passlogin.setInputType(129);
            } else {
                passlogin.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
            }
        }
    };
    public View.OnClickListener intentToRegister = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent in = new Intent(Login.this, Register.class);
            in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            Login.this.startActivity(in);
        }
    };

    public View.OnClickListener loginListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            doLoginManual();
        }
    };
    public void doLoginManual()
    {
        if (Utility.getInstance().isInternetOn(Login.this))
        {
            runDoLoginManual= true;
            String mEmail = emaillogin.getText().toString();
            String mPassword = passlogin.getText().toString();
            if(mEmail.length()==0||mPassword.length()==0){
                Toast.makeText(Login.this, "Email dan password harus diisi", Toast.LENGTH_SHORT).show();
            }
            else{
                pDialog = ProgressDialog.show(Login.this, "", "loading...");
                userController.postLogin(loginParams(mEmail, mPassword));
            }
        }
        else{
            Toast.makeText(Login.this, getResources().getString(R.string.active_your_internet), Toast.LENGTH_SHORT).show();
        }
    }
    public Map<String, String> loginParams(String email, String password) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("email", email);
        params.put("password", password);
        return  params;
    }
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    }
    @Override
    public void onResume() {
        super.onResume();

    }
    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed(){

        super.onBackPressed();
    }


    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
    }

    @Override
    public void onLoginSuccess(LoginCallback loginCallback) {
        int sukses = loginCallback.getSukses();
        String pesan = loginCallback.getPesan();
        GetUserData userData = loginCallback.getUserData();
        if(sukses==1) {
            setSession(userData);
            Intent i = new Intent(Login.this, MainMenuTab.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(i);
            Toast.makeText(Login.this, pesan , Toast.LENGTH_SHORT).show();

        }else{
            Toast.makeText(Login.this, pesan+", "+getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
        }
    }

    private void setSession(GetUserData userData){
        SessionManager sessionManager = new SessionManager(this);
        sessionManager.createLoginSession(
                userData.getUser_id(),
                userData.getFullname(),
                userData.getPhone(),
                userData.getAvatar(),
                userData.getEmail(),
                userData.getUserlevel()
        );

    }
    @Override
    public void onLoginError(APIErrorCallback apiErrorCallback) {
        if (apiErrorCallback.getError() != null) {
            pDialog.dismiss();
            Toast.makeText(Login.this, apiErrorCallback.getError()+", "+getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
        }
    }
}
