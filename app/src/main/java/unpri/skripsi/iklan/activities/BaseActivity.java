package unpri.skripsi.iklan.activities;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import unpri.skripsi.iklan.R;

/**
 * Created by edwin on 09/06/2016.
 */
public abstract class BaseActivity extends AppCompatActivity{
    private ProgressDialog mProgressDialog;
//    private SessionManager sessionManager;
//    private DBController dbController;
    private ProgressDialog progressDialog;
    private boolean runDeniedAccess = false;
    AlertDialog aDialog;
    private boolean isDialogShowing =false;
    private int currentRepeat = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        dbController = DBController.getInstance(this);
    }
    protected void showProgress(String msg) {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            dismissProgress();
        mProgressDialog = ProgressDialog.show(this, getResources().getString(R.string.app_name), msg);
    }

    protected void dismissProgress() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    public abstract void onReceiveBroadcast(String item_type);

    protected void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
    @Override
    protected void onPause() {
        super.onPause();
    }
    @Override
    protected void onStop() {
        super.onStop();
    }


    public static int RATE_DRIVER = 1;
//    public boolean setBookingDataViewInterface(){
//        return
//    }

}
