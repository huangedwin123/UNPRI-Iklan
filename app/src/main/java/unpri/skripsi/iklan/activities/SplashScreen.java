package unpri.skripsi.iklan.activities;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.Window;

import unpri.skripsi.iklan.R;

public class SplashScreen extends BaseLocationActivity {


    @Override
    final protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
    }


    public void intentToMainMenu(Location locationData) {
        Intent intent = new Intent(SplashScreen.this, MainMenuTab.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("latitude", locationData.getLatitude());
        intent.putExtra("longitude", locationData.getLongitude());
        startActivity(intent);
        finish();
    }
    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onGPSLocationReceived(Location locationData) {
        intentToMainMenu(locationData);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (aDialog != null) {
            aDialog.dismiss();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

}
