//package unpri.skripsi.iklan.activities;
//
//import android.os.Bundle;
//import android.support.v7.app.ActionBar;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.support.v7.widget.Toolbar;
//import android.view.View;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import java.util.ArrayList;
//
//import unpri.skripsi.iklan.R;
//import unpri.skripsi.iklan.adapters.SearchDoorsmeerAdapter;
//import unpri.skripsi.iklan.dao.DBController;
//import unpri.skripsi.iklan.models.IklanData;
//
///**
// * Created by edwin on 20/07/2017.
// */
//
//public class DoorsmeerFavoriteActivity extends BaseActivity{
//
//    private Toolbar toolbar;
//    private DoorsmeerFavoriteAdapter doorsmeerFavoriteAdapter;
//    private RecyclerView favoriteDoorsmeer;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.doorsmeer_favorite);
//        initToolbar();
//        renderViews();
//    }
//    private void renderViews(){
//        favoriteDoorsmeer = (RecyclerView) findViewById(R.id.menuList);
//        favoriteDoorsmeer.setHasFixedSize(true);
//        favoriteDoorsmeer.setNestedScrollingEnabled(false);
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
//        favoriteDoorsmeer.setLayoutManager(linearLayoutManager);
//        setListViewAdapter(getDataFavoriteFromDB());
//    }
//    private void initToolbar(){
//        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        if(toolbar!=null) {
//            setSupportActionBar(toolbar);
////            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
////                @Override
////                public void onClick(View v) {
////                    finish();
////                }
////            });
//        }
//        ActionBar actionBar = getSupportActionBar();
//        if(actionBar!=null) {
//            actionBar.setDisplayHomeAsUpEnabled(true);
//            getSupportActionBar().setTitle("Doorsmeer Detail");
//        }
//    }
//
//    private ArrayList<IklanData> getDataFavoriteFromDB(){
//        DBController dbController = new DBController(this);
//        ArrayList<IklanData> IklanDatas = dbController.getFavoriteDoorsmeer();
//        return IklanDatas;
//    }
//    private void setListViewAdapter(ArrayList<IklanData> IklanDatas){
//        if(doorsmeerFavoriteAdapter==null) {
//            doorsmeerFavoriteAdapter = new DoorsmeerFavoriteAdapter(DoorsmeerFavoriteActivity.this, IklanDatas);
//        }
//        favoriteDoorsmeer.setAdapter(doorsmeerFavoriteAdapter);
//    }
//    @Override
//    public void onReceiveBroadcast(String item_type) {
//
//    }
//}
