package unpri.skripsi.iklan.activities;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;

import unpri.skripsi.iklan.interfaceclass.OnGoogleConnectionListener;
import unpri.skripsi.iklan.utility.GPSTracker;

/**
 * Created by Huang on 4/23/2017.
 */

public abstract class BaseLocationActivity extends BaseActivity implements OnGoogleConnectionListener {
    private GPSTracker gpsTracker;
    private Handler handler;
    private static final long DELAYED_UPDATE_LOCATION = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public void onReceiveBroadcast(String item_type) {

    }
    @Override
    public void onGoogleConnected() {
        this.gpsTracker.requestLocation();
        checkLocationIsNull();
    }

    @Override
    public void onGoogleConnectionError(String errorMsg) {
    }

    @Override
    protected void onStart() {
        super.onStart();
        handler = new Handler();
        gpsTracker = GPSTracker.getInstance(BaseLocationActivity.this);
        gpsTracker.setOnGoogleConnectionListener(this);
    }
    @Override
    protected void onStop() {
        super.onStop();
    }
    @Override
    protected void onResume() {
        super.onResume();
        if(gpsTracker.isGoogleAPIConnected()) {
            checkLocationIsNull();
        }else{
            gpsTracker.connect();
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        if(gpsTracker.getAlertDialog()!=null){
            gpsTracker.getAlertDialog().dismiss();
        }
    }
    int count = 1;
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (!gpsTracker.isLocationEnabled(BaseLocationActivity.this)) {
                gpsTracker.showSettingsAlert(BaseLocationActivity.this);
                handler.removeCallbacks(runnable);
                count = 1;
            } else {
                Location location = getLastKnownLocation();
                if (count < 5) {
                    if (location != null) {
                        onGPSLocationReceived(location);
                        handler.removeCallbacks(runnable);
                        count = 1;
                    }else {
                        count++;
                        handler.postDelayed(this, DELAYED_UPDATE_LOCATION);
                    }
                } else if (count == 5) {
                    if (location != null) {
                        onGPSLocationReceived(location);
                    } else {
                        gpsTracker.showGPSHighAccuracyAlert(BaseLocationActivity.this);
                    }
                    handler.removeCallbacks(runnable);
                    count = 1;
                }
            }
        }
    };

    public abstract void onGPSLocationReceived(Location locationData);
    public Location getLastKnownLocation(){
        Location location = gpsTracker.getGoogleAPILocation();
        return location;
    }
    public void checkLocationIsNull(){
        if(handler!=null) {
            handler.postDelayed(runnable, DELAYED_UPDATE_LOCATION);
        }
    }
}

