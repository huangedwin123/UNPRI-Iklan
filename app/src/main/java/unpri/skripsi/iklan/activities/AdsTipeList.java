package unpri.skripsi.iklan.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import unpri.skripsi.iklan.R;
import unpri.skripsi.iklan.adapters.SearchDoorsmeerAdapter;
import unpri.skripsi.iklan.callbacks.AdsTipeListCallback;
import unpri.skripsi.iklan.controllers.DoorsmeerController;
import unpri.skripsi.iklan.interfaceclass.AdsTipeInterface;
import unpri.skripsi.iklan.io.APIErrorCallback;
import unpri.skripsi.iklan.models.IklanData;

/**
 * Created by Huang on 8/1/2017.
 */

public class AdsTipeList extends BaseActivity implements AdsTipeInterface{
    private ListView doorsmeer_list;
    private LinearLayout loadinglayout;
    private DoorsmeerController doorsmeerController;
    private SearchDoorsmeerAdapter searchDoorsmeerAdapter;
    private String id_tipe;
    private Toolbar toolbar;

    @Override
    public void onReceiveBroadcast(String item_type) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ads_tipe_list);
        initToolbar();
        doorsmeer_list = (ListView) findViewById(R.id.doorsmeer_list);
        doorsmeer_list.setOnItemClickListener(listItemClickListener);
        loadinglayout = (LinearLayout) findViewById(R.id.loadinglayout);
        doorsmeerController = DoorsmeerController.getInstance(AdsTipeList.this);
        doorsmeerController.getAdsTipeInterface(this);
        Intent i = getIntent();
        id_tipe = i.getStringExtra("id_tipe");
        getAdsTipe(id_tipe);
    }

    private AdapterView.OnItemClickListener listItemClickListener = new AdapterView.OnItemClickListener(){

        @Override
        public void onItemClick(AdapterView<?> parent, View view,
                                int position, long id) {
            IklanData IklanData = searchDoorsmeerAdapter.getItemdata().get(position);
            Intent detail = new Intent(AdsTipeList.this, DoorsmeerDetail.class);
            detail.putExtra("id", IklanData.id);
            startActivity(detail);
        }
    };

    public void SetListViewAdapter(ArrayList<IklanData> IklanDatas) {
        loadinglayout.setVisibility(View.GONE);
        if(searchDoorsmeerAdapter==null) {
            searchDoorsmeerAdapter = new SearchDoorsmeerAdapter(AdsTipeList.this);
        }
        searchDoorsmeerAdapter.removeNoResultView();
        searchDoorsmeerAdapter.clearItem();
        searchDoorsmeerAdapter.addItem(IklanDatas);
        doorsmeer_list.setAdapter(searchDoorsmeerAdapter);
    }
    private void initToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if(toolbar!=null) {
            setSupportActionBar(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Iklan Detail");
        }
    }
    private void getAdsTipe(String id_tipe){
        doorsmeerController.getAdsTipeList(searchDoorsmeerParams(id_tipe));
        return;
    }
    public Map<String, String> searchDoorsmeerParams(String id_tipe) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("id_tipe", id_tipe);
        return  params;
    }

    @Override
    public void onSuccessAdsTipe(AdsTipeListCallback adsTipeListCallback) {
        int sukses = adsTipeListCallback.getSukses();
        if(sukses==1){
            ArrayList<IklanData> IklanDatas = adsTipeListCallback.getAdsTipeList();
            if(IklanDatas.size()!=0){
                SetListViewAdapter(IklanDatas);
            }else{
                searchDoorsmeerAdapter.removeNoResultView();
                searchDoorsmeerAdapter.setNoResultView();
                searchDoorsmeerAdapter.clearItem();
            }
        }
    }

    @Override
    public void onErrorAdsTipe(APIErrorCallback apiErrorCallback) {

    }
}
