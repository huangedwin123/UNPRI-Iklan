package unpri.skripsi.iklan.activities.Account;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import unpri.skripsi.iklan.R;
import unpri.skripsi.iklan.activities.MainMenuTab;
import unpri.skripsi.iklan.callbacks.RegisterCallback;
import unpri.skripsi.iklan.controllers.UserController;
import unpri.skripsi.iklan.interfaceclass.OnRegisterListener;
import unpri.skripsi.iklan.io.APIErrorCallback;
import unpri.skripsi.iklan.utility.Utility;

/**
 * Created by Huang on 5/21/2017.
 */

public class Register extends AppCompatActivity implements OnRegisterListener{
    private TextView tvName;
    private TextView tvPassword;
    private TextView tvEmail;
    private Button btRegister      ;
    private Toolbar toolbar;
    private ProgressDialog pDialog;
    private UserController userController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        userController = UserController.getInstance(this);
        userController.setRegisterInterface(this);
        initToolbar();
        renderViews();
    }
    private void renderViews(){
        tvName = (TextView) findViewById(R.id.fullname);
        tvPassword= (TextView) findViewById(R.id.password);
        tvEmail= (TextView) findViewById(R.id.email);

        btRegister= (Button) findViewById(R.id.bt_register);
        btRegister.setOnClickListener(registerListener);
    }
    private void initToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if(toolbar!=null) {
            setSupportActionBar(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Register");
        }
    }

    private View.OnClickListener registerListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            doRegister();
        }
    };

    private void doRegister() {
        pDialog = ProgressDialog.show(Register.this, "", "Mendaftar...");
        UserController.getInstance(this).postRegister(registerParams());
        return;
    }

    private Map<String, String> registerParams(){
        String nama = tvName.getText().toString();
        String email = tvEmail.getText().toString();
        String password= tvPassword.getText().toString();
        if(Utility.getInstance().checkIfStringIsNotNullOrEmpty(nama)){
            tvName.setError("Nama perlu diisi.");
        }
        if(Utility.getInstance().checkIfStringIsNotNullOrEmpty(email)){
            tvEmail.setError("Email perlu diisi.");
        }
        if(Utility.getInstance().checkIfStringIsNotNullOrEmpty(password)){
            tvPassword.setError("Password perlu diisi.");
        }

        Map<String, String> map = new HashMap<>();
        map.put("nama",nama);
        map.put("email",email);
        map.put("password",password);
        return map;
    }
    @Override
    public void onRegisterSuccess(RegisterCallback registerCallback) {
        int sukses = registerCallback.getSukses();
        String pesan = registerCallback.getPesan();
        if(sukses==1){
            Intent i = new Intent(Register.this, Login.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(i);
            Toast.makeText(Register.this, pesan , Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(Register.this, pesan+", "+getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRegisterError(APIErrorCallback apiErrorCallback) {
        Toast.makeText(Register.this, getResources().getString(R.string.active_your_internet), Toast.LENGTH_SHORT).show();
    }
}
