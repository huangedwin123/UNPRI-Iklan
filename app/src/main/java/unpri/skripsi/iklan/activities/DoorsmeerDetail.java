package unpri.skripsi.iklan.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import unpri.skripsi.iklan.Config;
import unpri.skripsi.iklan.Constants;
import unpri.skripsi.iklan.R;
import unpri.skripsi.iklan.callbacks.DoorsmeerDetailCallback;
import unpri.skripsi.iklan.controllers.DoorsmeerController;
import unpri.skripsi.iklan.interfaceclass.DoorsmeerDetailInterface;
import unpri.skripsi.iklan.io.APIErrorCallback;
import unpri.skripsi.iklan.models.IklanData;
import unpri.skripsi.iklan.models.IklanDetailData;
import unpri.skripsi.iklan.utility.PiccassoUtility;

/**
 * Created by Huang on 7/17/2017.
 */

public class DoorsmeerDetail extends BaseActivity implements DoorsmeerDetailInterface{

    private Toolbar toolbar;
    private DoorsmeerController doorsmeerController;
    private TextView tvAdsAddress;
    private TextView tvAdsDescription;
    private TextView tvAdsType;
    private ImageView tvAdsPhoto;
    private TextView tvCompanyName;
    private TextView tvCompanyAddress;
    private TextView tvCompanyPhone;
    private TextView tvCompanyDescription;
    private ImageView tvCompanyLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doorsmeer_detail);
        doorsmeerController = DoorsmeerController.getInstance(DoorsmeerDetail.this);
        doorsmeerController.setDoorsmeerDetailInterface(this);
        initToolbar();
        renderViews();
        Intent i = getIntent();
        String id = i.getStringExtra("id");
        getDoorsmeerDetail(id);
    }

    private void setDataFromAPI(IklanDetailData IklanDetailDatas) {
        tvAdsAddress.setText(IklanDetailDatas.alamat);
        tvAdsDescription.setText(IklanDetailDatas.deskripsi_iklan);
        tvAdsType.setText(IklanDetailDatas.nama_jenis);

        PiccassoUtility.loadImage(this, Config.APP_IMAGE_URL+IklanDetailDatas.cover_image, tvAdsPhoto);

        tvCompanyName.setText(IklanDetailDatas.nama_perusahaan);
        tvCompanyAddress.setText(IklanDetailDatas.alamat);
        tvCompanyPhone.setText(IklanDetailDatas.nomor_telepon);
        tvCompanyDescription.setText(IklanDetailDatas.deskripsi);
        tvCompanyDescription.setVisibility(View.GONE);
        PiccassoUtility.loadImage(this,Config.APP_IMAGE_URL+ IklanDetailDatas.logo_perusahaan, tvCompanyLogo);
    }

    private void renderViews() {
        tvAdsAddress= (TextView) findViewById(R.id.ads_address);
        tvAdsDescription= (TextView) findViewById(R.id.ads_description);
        tvAdsType= (TextView) findViewById (R.id.ads_type);

        tvAdsPhoto= (ImageView) findViewById(R.id.ads_photo);

        tvCompanyName= (TextView) findViewById(R.id.company_name);
        tvCompanyAddress= (TextView) findViewById(R.id.company_address);
        tvCompanyPhone= (TextView) findViewById (R.id.company_phone);
        tvCompanyDescription= (TextView) findViewById (R.id.company_description);
        tvCompanyLogo= (ImageView) findViewById(R.id.company_logo);
    }

    private void initToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if(toolbar!=null) {
            setSupportActionBar(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Iklan Detail");
        }
    }
    private void getDoorsmeerDetail(String id){

        doorsmeerController.getAdsDetail(searchDoorsmeerParams(id));
        return;
    }
    public Map<String, String> searchDoorsmeerParams(String id) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("id", id);
        return  params;
    }
    @Override
    public void onReceiveBroadcast(String item_type) {

    }

    @Override
    public void onSuccessGetDoorsmeerDetail(DoorsmeerDetailCallback doorsmeerDetailCallback) {
        int sukses = doorsmeerDetailCallback.getSukses();
        if(sukses==1){
            IklanDetailData IklanDetailDatas = doorsmeerDetailCallback.getData();
            if(doorsmeerDetailCallback!=null){
                setDataFromAPI(IklanDetailDatas);
            }
        }
    }

    @Override
    public void onErrorGetDoorsmeerDetail(APIErrorCallback apiErrorCallback) {

    }
}
