package unpri.skripsi.iklan.activities;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import unpri.skripsi.iklan.R;
import unpri.skripsi.iklan.fragments.DoorsmeerMapFragment;
import unpri.skripsi.iklan.fragments.DoorsmeerSearchFragment;
import unpri.skripsi.iklan.fragments.ProfileFragment;

/**
 * Created by Huang on 4/23/2017.
 */

public class MainMenuTab extends BaseActivity implements ViewPager.OnPageChangeListener{

    private Toolbar toolbar;
    private ViewPager viewPager;
    private MenuItem item;
    private BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bottom_navigation_activity);
        initToolbar();
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_profile:
                                viewPager.setCurrentItem(0);
                                break;
                            case R.id.action_maps:
                                viewPager.setCurrentItem(1);
                                break;
                            case R.id.action_search:
                                viewPager.setCurrentItem(2);
                                break;

                        }
                        return true;
                    }
                });
        Intent i =getIntent();
        double latitude = i.getDoubleExtra("latitude",0.0);
        double longitude = i.getDoubleExtra("longitude",0.0);
        setupViewPager(viewPager, latitude, longitude);
    }

    @Override
    public void onReceiveBroadcast(String item_type) {

    }
    private void initToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if(toolbar!=null) {
            setSupportActionBar(toolbar);
//            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    finish();
//                }
//            });
        }
        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null) {
            getSupportActionBar().setTitle(getResources().getString(R.string.app_name));
        }
    }
    private int[] tabIcons = {
            R.mipmap.ic_launcher,
            R.mipmap.ic_launcher,
            R.mipmap.ic_launcher
    };
    private void setupViewPager(ViewPager viewPager, double latitude, double longitude) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        Fragment doorsmeerMapFragment = new DoorsmeerMapFragment();
        Bundle bundle = new Bundle();
        bundle.putDouble("latitude", latitude);
        bundle.putDouble("longitude", longitude);
        doorsmeerMapFragment.setArguments(bundle);
        adapter.addFragment(new ProfileFragment(),/*getResources().getString(R.string.booking_complete)*/"C");
        adapter.addFragment(doorsmeerMapFragment, /*getResources().getString(R.string.booking_progress)*/"A");
        adapter.addFragment(new DoorsmeerSearchFragment(),/*getResources().getString(R.string.booking_complete)*/"B");

        viewPager.setAdapter(adapter);
    }
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if(position == 0){
            item.setVisible(false);
        }
    }
    @Override
    public void onPageSelected(int position) {
        if(position == 0){
            item.setVisible(false);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    /*@Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        item = menu.findItem(R.id.action_filter);

        return super.onPrepareOptionsMenu(menu);
    }*/
    @Override
    protected void onResume() {
        super.onResume();
    }
    @Override
    protected void onPause() {
        super.onPause();
    }



    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }
}
