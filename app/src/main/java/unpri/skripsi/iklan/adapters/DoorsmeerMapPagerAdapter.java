package unpri.skripsi.iklan.adapters;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import unpri.skripsi.iklan.R;
import unpri.skripsi.iklan.models.IklanData;
import unpri.skripsi.iklan.utility.PiccassoUtility;

/**
 * Created by User on 6/11/2017.
 */

public class DoorsmeerMapPagerAdapter extends PagerAdapter {
    private Context mContext;
    private List<IklanData> IklanDatas;
    public DoorsmeerMapPagerAdapter(Context mContext, List<IklanData> orphanageDatas){
        this.mContext = mContext;
        this.IklanDatas = orphanageDatas;
    }
    public int getCount() {
        return IklanDatas.size();
    }
    private ImageView ivOrphanageCover;
    private TextView tvOrphanageName, tvOrphanageAddress, tvChildren, tvBathroom, tvFloor, tvDescription;

    public Object instantiateItem(ViewGroup collection, int position) {
        LayoutInflater inflater = (LayoutInflater) collection.getContext()
                .getSystemService(mContext.LAYOUT_INFLATER_SERVICE);

        int resId = R.layout.doorsmeer_maplist_item;
        View view = inflater.inflate(resId, null);
//        ivOrphanageCover = (ImageView) view.findViewById(R.id.ivOrphanageCover);
//        tvOrphanageName = (TextView) view.findViewById(R.id.tvOrphanageName);
//        tvOrphanageAddress = (TextView) view.findViewById(R.id.tvOrphanageAddress);
//        tvChildren = (TextView) view.findViewById(R.id.tvChildren);
//        tvBathroom = (TextView) view.findViewById(R.id.tvBathroom);
//        tvFloor = (TextView) view.findViewById(R.id.tvFloor);
//        tvDescription = (TextView) view.findViewById(R.id.tvDescription);
//        PiccassoUtility.loadImage(mContext, IklanDatas.get(position).coverImage, ivOrphanageCover);
//        tvOrphanageName.setText(orphanageDatas.get(position).name);
//        tvOrphanageAddress.setText(orphanageDatas.get(position).address);
//        tvChildren.setText(String.valueOf(orphanageDatas.get(position).totalChild));
//        tvBathroom.setText(String.valueOf(orphanageDatas.get(position).bathroom));
//        tvFloor.setText(String.valueOf(orphanageDatas.get(position).floor));
//        tvDescription.setText(orphanageDatas.get(position).description);
//        ((ViewPager) collection).addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup arg0, int arg1, Object arg2) {
        ((ViewPager) arg0).removeView((View) arg2);
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == ((View) arg1);
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}