package unpri.skripsi.iklan.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import unpri.skripsi.iklan.R;
import unpri.skripsi.iklan.activities.Account.Login;
import unpri.skripsi.iklan.activities.Account.Register;
import unpri.skripsi.iklan.activities.AdsTipeList;
import unpri.skripsi.iklan.activities.MainMenuTab;
import unpri.skripsi.iklan.models.ProfileMenuData;
import unpri.skripsi.iklan.utility.SessionManager;
import unpri.skripsi.iklan.utility.Utility;

/**
 * Created by edwin on 19/06/2017.
 */

public class ProfileMenuAdapter extends RecyclerView.Adapter<ProfileMenuAdapter.SingleItemRowHolder> {
    private List<ProfileMenuData> data;
    private Context mContext;
    private Activity mActivity;
    public ProfileMenuAdapter(Context context, List<ProfileMenuData> profileMenuDatas) {
        data = profileMenuDatas;
        this.mContext = context;
        mActivity = (Activity) mContext;
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.profile_menu_item, null);
        SingleItemRowHolder mh = new SingleItemRowHolder(v);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        v.setLayoutParams(lp);
        return mh;
    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, int i) {
        final ProfileMenuData singleItem = data.get(i);
        holder.menuLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(singleItem.getMenuName().equals("Billboard")){
                    Intent i = new Intent(mActivity, AdsTipeList.class);
                    i.putExtra("id_tipe","1");
                    mActivity.startActivity(i);
                }else if(singleItem.getMenuName().equals("Baliho")){
                    Intent i = new Intent(mActivity, AdsTipeList.class);
                    i.putExtra("id_tipe","2");
                    mActivity.startActivity(i);
                }else if(singleItem.getMenuName().equals("TV")){
                    Intent i = new Intent(mActivity, AdsTipeList.class);
                    i.putExtra("id_tipe","3");
                    mActivity.startActivity(i);
                }
            }
        });
        holder.tv_menu_name.setText(singleItem.getMenuName());
        holder.iv_menu_image.setImageResource(singleItem.getMenuIcon());
    }

    @Override
    public int getItemCount() {
            return (null != data ? data.size() : 0);
            }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {
        protected TextView tv_menu_name;
        protected ImageView iv_menu_image;
        protected LinearLayout menuLayout;

        public SingleItemRowHolder(View view) {
            super(view);
            this.menuLayout = (LinearLayout) view.findViewById(R.id.menuLayout);
            this.tv_menu_name = (TextView) view.findViewById(R.id.menuName);
            this.iv_menu_image = (ImageView) view.findViewById(R.id.menuImage);
        }
    }
}