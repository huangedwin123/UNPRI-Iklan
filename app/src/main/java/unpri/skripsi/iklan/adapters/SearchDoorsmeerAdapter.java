package unpri.skripsi.iklan.adapters;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import unpri.skripsi.iklan.Config;
import unpri.skripsi.iklan.R;
import unpri.skripsi.iklan.dao.DBController;
import unpri.skripsi.iklan.models.IklanData;
import unpri.skripsi.iklan.utility.PiccassoUtility;
import unpri.skripsi.iklan.utility.SessionManager;

/**
 * Created by Huang on 6/18/2017.
 */

public class SearchDoorsmeerAdapter extends BaseAdapter {
    ArrayList<IklanData> searchIklanDatas;
    DBController dbController;
    Context context;
    private static LayoutInflater inflater = null;

    SessionManager session;
    String userId;

    private ArrayList<Integer> itemType = new ArrayList<Integer>();

    private String id_favorite;

    public SearchDoorsmeerAdapter(Context context) {
        dbController = new DBController(context);
        //------------------------------------Session--------------------------------------------------------
        session = new SessionManager(context);
        HashMap<String,String> map = session.getUserDetails();
        userId = map.get(session.KEY_ID);
        //---------------------------------------------------------------------------------------------------
        searchIklanDatas = new ArrayList<>();
        this.context = context;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    public ArrayList<IklanData> getItemdata() {
        return searchIklanDatas;
    }
    static class ViewHolder {
        public TextView no_result_txt;
        public ImageView ivDoorsmeerCover;
        public TextView tvDoorsmeerName;
        public TextView tvDoorsmeerAddress;
        public TextView tvAdsDescription;
    }
    public static final int TYPE_ITEM = 0;
    public static final int TYPE_HISTORY_ITEM = 1;
    public static final int TYPE_SEPARATOR = 2;
    public static final int TYPE_NO_RESULT = 3;

    @Override
    public int getItemViewType(int position) {
        return itemType.get(position);
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    public void addItem(ArrayList<IklanData> searchIklanDatas) {
        for(int i = 0; i<searchIklanDatas.size(); i++) {
            this.searchIklanDatas.add(searchIklanDatas.get(i));
            itemType.add(TYPE_ITEM);
        }
    }
    public void clearItem() {
        if(searchIklanDatas!=null){
            for(int i = 0; i<searchIklanDatas.size();) {
                int rowType = getItemViewType(i);
                if(rowType==TYPE_ITEM){
                    searchIklanDatas.remove(i);
                    itemType.remove(i);
                }else{
                    i++;
                }
            }
        }
    }

    public void setNoResultView() {
        IklanData IklanData = new IklanData();
        IklanData.alamat = "No Result";
        searchIklanDatas.add(IklanData);
        itemType.add(TYPE_NO_RESULT);
    }
    public void removeNoResultView() {
        int noResultPosition = itemType.indexOf(TYPE_NO_RESULT);
        if(noResultPosition!=-1) {
            itemType.remove(noResultPosition);
            searchIklanDatas.remove(noResultPosition);
        }

    }
  
    @Override
    public int getCount() {
        return searchIklanDatas.size();
    }

    @Override
    public IklanData getItem(int i) {
        return searchIklanDatas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        int rowType = getItemViewType(position);
        if (convertView == null){
            holder = new ViewHolder();
            switch (rowType) {
                case TYPE_ITEM:
                    convertView = inflater.inflate(R.layout.doorsmeer_maplist_item, null);
                    holder.ivDoorsmeerCover = (ImageView) convertView.findViewById(R.id.ivAdsCover);
                    holder.tvDoorsmeerName = (TextView) convertView.findViewById(R.id.tvPerusahaan);
                    holder.tvDoorsmeerAddress = (TextView) convertView.findViewById(R.id.tvAdsAddress);
                    holder.tvAdsDescription = (TextView) convertView.findViewById(R.id.tvAdsDescription);
                    break;
                case TYPE_NO_RESULT:
                    convertView = inflater.inflate(R.layout.no_result, null);
                    holder.no_result_txt = (TextView) convertView.findViewById(R.id.no_result_txt);
                    break;
            }

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        switch (rowType) {
            case TYPE_ITEM:
                PiccassoUtility.loadImage(context,  Config.APP_IMAGE_URL+searchIklanDatas.get(position).cover_image, holder.ivDoorsmeerCover);
                holder.tvDoorsmeerName.setText(searchIklanDatas.get(position).nama_perusahaan);
                holder.tvDoorsmeerAddress.setText(searchIklanDatas.get(position).alamat);
                holder.tvAdsDescription.setText(searchIklanDatas.get(position).deskripsi_iklan);

                break;

            case TYPE_NO_RESULT:
                holder.no_result_txt.setText("Tidak ada Hasil");
                break;
        }
        return convertView;
    }
}
