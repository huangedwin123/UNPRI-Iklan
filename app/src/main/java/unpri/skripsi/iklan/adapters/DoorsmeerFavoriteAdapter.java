//package unpri.skripsi.iklan.adapters;
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.Intent;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import unpri.skripsi.iklan.R;
//import unpri.skripsi.iklan.activities.Account.Register;
//import unpri.skripsi.iklan.activities.MainMenuTab;
//import unpri.skripsi.iklan.dao.DBController;
//import unpri.skripsi.iklan.models.IklanData;
//import unpri.skripsi.iklan.models.ProfileMenuData;
//import unpri.skripsi.iklan.utility.PiccassoUtility;
//import unpri.skripsi.iklan.utility.SessionManager;
//
///**
// * Created by edwin on 20/07/2017.
// */
//
//public class DoorsmeerFavoriteAdapter extends RecyclerView.Adapter<DoorsmeerFavoriteAdapter.SingleItemRowHolder> {
//    private final DBController dbController;
//    private List<IklanData> data;
//    private Context mContext;
//    private Activity mActivity;
//    ArrayList<IklanData> searchIklanDatas;
//
//    public DoorsmeerFavoriteAdapter(Context context, List<IklanData> IklanDatas) {
//        data = IklanDatas;
//        this.mContext = context;
//        mActivity = (Activity) mContext;
//        dbController = new DBController(context);
//    }
//
//    @Override
//    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
//        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.doorsmeer_maplist_item, null);
//        SingleItemRowHolder mh = new SingleItemRowHolder(v);
//        return mh;
//    }
//
//    @Override
//    public void onBindViewHolder(SingleItemRowHolder holder, int position) {
//        final IklanData singleItem = data.get(position);
//        if (singleItem.favorite.equals("false")) {
//            holder.favorite.setImageResource(R.drawable.ic_favorite_off);
//        } else {
//            holder.favorite.setImageResource(R.drawable.ic_favorite_on);
//        }
//        PiccassoUtility.loadImage(mContext, singleItem.coverImage, holder.favorite);
//        holder.tvDoorsmeerName.setText(singleItem.doorsmeer_name);
//        holder.tvDoorsmeerAddress.setText(singleItem.alamat);
//
//        holder.favorite.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (singleItem.favorite.equals("false")) {
//                    dbController.openDataBase();
//                    long id = dbController.insertFavoriteDoorsmeer(singleItem);
//                    dbController.close();
//                    singleItem.favorite = "true";
//                    notifyDataSetChanged();
//                } else {
//                    dbController.openDataBase();
//                    dbController.deleteFavoriteDoorsmeer(singleItem.id);
//                    dbController.close();
//                    singleItem.favorite = "false";
//                    notifyDataSetChanged();
//                }
//            }
//        });
//    }
//
//    @Override
//    public int getItemCount() {
//        return (null != data ? data.size() : 0);
//    }
//
//    public class SingleItemRowHolder extends RecyclerView.ViewHolder {
//        ImageView favorite;
//        public TextView ivDoorsmeerCover;
//        public TextView tvDoorsmeerName;
//        public TextView tvDoorsmeerAddress;
//        public LinearLayout favoriteLayout;
//
//
//        public SingleItemRowHolder(View view) {
//            super(view);
//            this.ivDoorsmeerCover = (TextView) view.findViewById(R.id.ivDoorsmeerCover);
//            this.tvDoorsmeerName = (TextView) view.findViewById(R.id.tvDoorsmeerName);
//            this.tvDoorsmeerAddress = (TextView) view.findViewById(R.id.tvDoorsmeerAddress);
//            this.favoriteLayout = (LinearLayout) view.findViewById(R.id.favoritelayout);
//            this.favorite = (ImageView) view.findViewById(R.id.favorite);
//        }
//    }
//}